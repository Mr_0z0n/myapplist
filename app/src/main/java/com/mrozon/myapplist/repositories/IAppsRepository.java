package com.mrozon.myapplist.repositories;

import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;

import com.mrozon.myapplist.db.model.App;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public interface IAppsRepository {
    Observable<App> getNonSystemApps();

    Flowable<List<App>> getNonSystemApps2();

    Drawable getLogoApp(String packageName);
}
