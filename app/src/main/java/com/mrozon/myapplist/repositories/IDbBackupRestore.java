package com.mrozon.myapplist.repositories;

public interface IDbBackupRestore {
    void backup(String file);

    void restore(String backup_file);
}
