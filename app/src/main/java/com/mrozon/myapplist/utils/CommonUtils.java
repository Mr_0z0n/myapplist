package com.mrozon.myapplist.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Environment;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.mrozon.myapplist.R;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

import static com.mrozon.myapplist.utils.AppConstants.TIMESTAMP_FILE_FORMAT;


public final class CommonUtils {

    private static final String TAG = "CommonUtils";

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }


    public static Calendar toCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static String getTimeStamp(Date date) {
//        return new SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT, Locale.US).format(date);
        return new SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT, Locale.getDefault()).format(date);
    }

    public static class CircleTransform implements Transformation {

        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    public static long convert_date(String s) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date result =  df.parse(s);
        return result.getTime();
    }

    public static File getScreenShot(Activity activity, View view, Boolean isLocal){
        //get root view from current activity
        View rootView = Objects.requireNonNull(activity).getWindow().getDecorView().getRootView();
        //capture the root view
        rootView.setDrawingCacheEnabled(true);
        Bitmap bm = Bitmap.createBitmap(rootView.getDrawingCache());
        rootView.setDrawingCacheEnabled(false);
        //store the Bitmap into the SDCard
//        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Screenshots";
        String dirPath = Environment.getExternalStoragePublicDirectory("TemperMeasure").getAbsolutePath()+ "/Screenshots";
        //dirPath = getBaseContext().getDatabasePath("screenshots").getPath().replace("databases/","");
        //getBaseContext().getDatabasePath("").getPath();
        File dir = new File(dirPath);
        ///data/data/com.mrozon.tempermeasure.free/databases/333
        if(!dir.exists())
            dir.mkdirs();
        String imageName = "";
        if(isLocal)
            imageName = String.format("graphics_%s.jpg",
                    new SimpleDateFormat(TIMESTAMP_FILE_FORMAT).format(new Date()));
        else
            imageName = "shared_graphics.jpg";
        File file = new File(dirPath, imageName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(isLocal) {
            Snackbar.make(Objects.requireNonNull(view), R.string.save_screenshot_done, Snackbar.LENGTH_LONG)
                    .show();
        }
        return file;
    }


    public static class Variable<T>{

        private T value;
        private Subject<T> observable;

        public Variable(T defaultValue) {
            this.value = defaultValue;
//            this.observable = BehaviorSubject.create();
            this.observable = BehaviorSubject.createDefault(defaultValue);
        }

        public T getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
            this.observable.onNext(value);
        }

        public Subject<T> getObservable() {
            return this.observable;
        }
    }

}
