package com.mrozon.myapplist.ui.fragment.app;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.arellomobile.mvp.MvpAppCompatDialogFragment;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.model.App;
import com.mrozon.myapplist.presentation.view.app.EditAppDialogView;
import com.mrozon.myapplist.presentation.presenter.app.EditAppDialogPresenter;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class EditAppDialogFragment extends MvpAppCompatDialogFragment implements EditAppDialogView {
    public static final String TAG = "EditAppDialogFragment";

    @InjectPresenter
    EditAppDialogPresenter mEditAppPresenter;

    @BindView(R.id.etCommentApp)
    EditText etCommentApp;

    private static CommentListener commentListener;

    public interface CommentListener {
        void onEditComplete(String comment);
    }

    public static EditAppDialogFragment newInstance(CommentListener commentListener, String comment) {

        EditAppDialogFragment.commentListener = commentListener;

        EditAppDialogFragment fragment = new EditAppDialogFragment();

        Bundle args = new Bundle();
        args.putString("comment",comment);
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        String comment = "";
        if(getArguments()!=null) {
            comment = getArguments().getString("comment");
        }

        LayoutInflater factory = LayoutInflater.from(getContext());
        final View view = factory.inflate(R.layout.fragment_edit_app_dialog, null);
        ButterKnife.bind(this, view);

        etCommentApp.setText(comment);

        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()), R.style.Theme_AppCompat_Light_Dialog_Alert)
                .setTitle(R.string.input_comment_app)
                .setView(view)
                .setCancelable(true)
                .setPositiveButton(R.string.ok, (dialog1, which) -> {
                    if(commentListener!=null)
                        commentListener.onEditComplete(etCommentApp.getText().toString());
                });

        return builder.create();

    }

}
