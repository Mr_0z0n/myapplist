package com.mrozon.myapplist.presentation.presenter.backup;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.presentation.view.backup.RestoreBackupView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mrozon.myapplist.repositories.DbBackupRestore;
import com.snatik.storage.Storage;

import java.io.File;

import durdinapps.rxfirebase2.RxFirebaseStorage;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.mrozon.myapplist.db.MyAppListDatabase.DB_NAME;

@InjectViewState
public class RestoreBackupPresenter extends MvpPresenter<RestoreBackupView> {

    public void restoreLocalBackup(Context context) {
        DialogProperties properties = new DialogProperties();
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = new String[]{"backup"};
        FilePickerDialog dialog = new FilePickerDialog(context, properties);
        dialog.setTitle(context.getResources().getString(R.string.select_file_backup));
        dialog.setPositiveBtnName(context.getResources().getString(R.string.ok));
        dialog.setNegativeBtnName(context.getResources().getString(R.string.cancel));
        dialog.setDialogSelectionListener(files -> {
            if(files.length<1){
                getViewState().showMessage(context.getResources().getString(R.string.backup_file_not_select));
            }
            else {
                String backup_file = files[0];
                Disposable subscribe = Completable.fromAction(() -> DbBackupRestore.getInstance().restore(backup_file))
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(__ -> getViewState().showProgress())
                        .subscribe(() -> {
                                    getViewState().hideProgress();
                                    getViewState().showMessage(context.getString(R.string.restore_success));
                                },
                                (throwable -> {
                                    getViewState().hideProgress();
                                    getViewState().showMessage(throwable.getMessage());
                                }));
            }
        });
        dialog.show();
    }

    public void restoreCloudBackup() {
        if(FirebaseAuth.getInstance().getUid()==null)
        {
            getViewState().showMessage(R.string.firebase_auth_null);
            return;
        }
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        Storage storage = new Storage(MyApplication.INSTANCE);
        String dir_app_name = MyApplication.INSTANCE.getString(R.string.dir_app_name);

        String dirPath = Environment.getExternalStoragePublicDirectory(dir_app_name).getAbsolutePath()+ "/Share";
        if(!storage.isDirectoryExists(dirPath)){
            storage.createDirectory(dirPath);
        }
        Uri backup_temp_file =  Uri.fromFile(new File(String.format("%s/%s.backup", dirPath, DB_NAME)));
        String backup_temp = String.valueOf(backup_temp_file).replaceFirst("file://","");
        String db_file = String.valueOf(MyApplication.INSTANCE.getDatabasePath(DB_NAME));
        ///sdcard/TemperMeasure/Share/db_measure.backup
        StorageReference riversRef = storageReference.child(String.format("user/%s/backup/%s.backup", FirebaseAuth.getInstance().getUid(), DB_NAME));
        Disposable subscribe = RxFirebaseStorage.getFile(riversRef, backup_temp_file)

                .flatMap(taskSnapshot -> Single.fromCallable(() -> {
                    DbBackupRestore.getInstance().restore(backup_temp);
                    return new Object();
                }).subscribeOn(Schedulers.computation()))
//                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe((result) -> {
                    getViewState().hideProgress();
                    getViewState().showMessage(R.string.restore_success);
                }, throwable -> {
                    getViewState().hideProgress();
                    Timber.e(throwable, "restoreCloudBackup");
                    getViewState().showMessage(throwable.toString());
                });
    }
}
