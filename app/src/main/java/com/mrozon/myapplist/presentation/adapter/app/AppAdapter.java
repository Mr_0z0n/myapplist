package com.mrozon.myapplist.presentation.adapter.app;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.model.App;
import com.mrozon.myapplist.db.model.CategoryWithCount;
import com.mrozon.myapplist.db.model.MyNativeContentAd;
import com.mrozon.myapplist.presentation.holder.app.AppViewHolder;
import com.mrozon.myapplist.presentation.holder.app.NativeBannerViewHolder;
import com.mrozon.myapplist.presentation.holder.category.CategoryWithCountHolder;
import com.mrozon.myapplist.utils.rvpattern.IBaseListItem;
import com.mrozon.myapplist.utils.rvpattern.RVAdapter;

import java.util.ArrayList;

import timber.log.Timber;

public class AppAdapter extends RVAdapter {

    private ClickListener onClickListener;

    public interface ClickListener {
        void onLogoClick(App app);
        void onFavoriteClick(App app);
        void onShareClick(App app);
        void onCommentClick(App app);
        void onChooseCategory(App app);
        void onUninstallApp(App app);
        void onDownloadApp(App app);
    }

    public ArrayList<IBaseListItem> getAll(){
        return items;
    }


    public AppAdapter(ClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        return new AppViewHolder(inflateByViewType(MyApplication.INSTANCE,viewType,parent));
        switch (viewType){
            case R.layout.item_app:
                return new AppViewHolder(inflateByViewType(MyApplication.INSTANCE,viewType,parent));
            case R.layout.widget_native_template:
                return new NativeBannerViewHolder(inflateByViewType(MyApplication.INSTANCE,viewType,parent));
            default:
                throw new IllegalStateException("There is no match with current layoutId");
//                return new IndicatorViewHolder(inflateByViewType(context,i,viewGroup));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(onClickListener==null) {
            Timber.e("AppAdapter: Error onClickListener==null!!!!");
            throw new Error("AppAdapter: Error onClickListener==null!!!!");
        }
        if (holder instanceof AppViewHolder){
            App app = (App)items.get(position);
            AppViewHolder appViewHolder = (AppViewHolder)holder;
            appViewHolder.app_label.setText(app.title);
            appViewHolder.app_package.setText(app.fullPackage);
            appViewHolder.app_category.setText(String.format("#%s", app.categoryTitle));
            if(app.comment.isEmpty())
                appViewHolder.app_image_comments.setImageResource(R.drawable.ic_speaker_notes_off_cyan_300_24dp);
            else
                appViewHolder.app_image_comments.setImageResource(R.drawable.ic_speaker_notes_cyan_900_24dp);
            if(app.favorite)
                appViewHolder.app_image_favorite.setImageResource(R.drawable.ic_bookmark_red_900_48dp);
            else
                appViewHolder.app_image_favorite.setImageResource(R.drawable.ic_bookmark_border_red_900_48dp);
            if(app.isExisted)
            {
                appViewHolder.app_image.setImageDrawable(app.img);
                appViewHolder.app_label.setPaintFlags(appViewHolder.app_label.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            }
            else
            {
                appViewHolder.app_image.setImageResource(R.drawable.ic_shop_black_48dp);
                appViewHolder.app_label.setPaintFlags(appViewHolder.app_label.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            appViewHolder.app_image_delete.setVisibility(app.isExisted?View.VISIBLE:View.INVISIBLE);
            appViewHolder.app_image_download.setVisibility(app.isExisted?View.VISIBLE:View.INVISIBLE);
            appViewHolder.app_image_download.setOnClickListener(l->onClickListener.onDownloadApp(app));
            appViewHolder.app_image_delete.setOnClickListener(l->onClickListener.onUninstallApp(app));
            appViewHolder.app_image.setOnClickListener(l->onClickListener.onLogoClick(app));
            appViewHolder.app_image_favorite.setOnClickListener(l->onClickListener.onFavoriteClick(app));
            appViewHolder.app_image_share.setOnClickListener(l->onClickListener.onShareClick(app));
            appViewHolder.app_image_comments.setOnClickListener(l->onClickListener.onCommentClick(app));
            appViewHolder.app_category.setOnClickListener(l->onClickListener.onChooseCategory(app));
        }
        if (holder instanceof NativeBannerViewHolder) {
            NativeBannerViewHolder nativeBannerViewHolder = (NativeBannerViewHolder)holder;
            MyNativeContentAd myNativeContentAd = (MyNativeContentAd)items.get(position);
            nativeBannerViewHolder.nativeBannerView.setVisibility(View.GONE);

            nativeBannerViewHolder.nativeBannerView.setAd(myNativeContentAd.getNativeContentAd());
            nativeBannerViewHolder.nativeBannerView.setVisibility(View.VISIBLE);
            myNativeContentAd.getNativeContentAd().loadImages();
        }
    }
}
