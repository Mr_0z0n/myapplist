package com.mrozon.myapplist.presentation.view.category;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mrozon.myapplist.db.model.Category;
import com.mrozon.myapplist.db.model.CategoryWithCount;

import java.util.List;

public interface ShowCategoryView extends MvpView {

    @StateStrategyType(SkipStrategy.class)
    void showMessage(String message);

    @StateStrategyType(SkipStrategy.class)
    void showMessage(int res_message);

    void showProgress();

    void hideProgress();

    void updateList(List<CategoryWithCount> categories);

}
