package com.mrozon.myapplist.presentation.view.app;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mrozon.myapplist.db.model.App;
import com.mrozon.myapplist.db.model.Category;
import com.mrozon.myapplist.db.model.CategoryWithCount;
import com.yandex.mobile.ads.nativeads.NativeContentAd;

import java.util.List;

public interface ShowAppView extends MvpView {

    @StateStrategyType(SkipStrategy.class)
    void showMessage(String message);

    @StateStrategyType(SkipStrategy.class)
    void showMessage(int res_message);

    void showProgress();

    void hideProgress();

    void updateList(List<App> categories);

    void changeFavoriteIcon(Boolean value);

    void changeInstallAppIcon(Boolean value);

    void showMessageWithOpenAction(int res_message, String path);

    void loadAd(NativeContentAd nativeContentAd);
}
