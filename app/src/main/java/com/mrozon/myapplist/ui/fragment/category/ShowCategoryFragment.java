package com.mrozon.myapplist.ui.fragment.category;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.model.Category;
import com.mrozon.myapplist.db.model.CategoryWithCount;
import com.mrozon.myapplist.presentation.adapter.category.CategoryAdapter;
import com.mrozon.myapplist.presentation.view.category.ShowCategoryView;
import com.mrozon.myapplist.presentation.presenter.category.ShowCategoryPresenter;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class ShowCategoryFragment extends MvpAppCompatFragment implements ShowCategoryView {
    public static final String TAG = "ShowCategoryFragment";

    @BindView(R.id.rvItems)
    RecyclerView rvItems;

    @BindView(R.id.fabAddCategory)
    FloatingActionButton fabAddCategory;

    @BindView(R.id.srlItems)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @InjectPresenter
    ShowCategoryPresenter mShowCategoryPresenter;

    private CategoryAdapter adapter;

    public static ShowCategoryFragment newInstance() {
        ShowCategoryFragment fragment = new ShowCategoryFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_category, container, false);
        ButterKnife.bind(this, view);

        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity()))
                .getSupportActionBar()).setTitle(R.string.app_cats);

        adapter = new CategoryAdapter(
        //delete
        this::showPromptDelete,
        //rename
        this::showPromptRename);
        LinearLayoutManager layout = new LinearLayoutManager(getContext());
        rvItems.setLayoutManager(layout);
        rvItems.setAdapter(adapter);

        mShowCategoryPresenter.getItems();

        mSwipeRefreshLayout.setOnRefreshListener(()->mShowCategoryPresenter.getItems());

        fabAddCategory.setOnClickListener(l->{
            EditCategoryDialogFragment editCategoryDialogFragment = EditCategoryDialogFragment.newInstance(
                    (category_id, text) -> mShowCategoryPresenter.addCategory(text), 0, "");
            editCategoryDialogFragment.show(getActivity().getSupportFragmentManager(),EditCategoryDialogFragment.TAG);
        });

        return view;
    }

    private void showPromptRename(int category_id, String text) {
        EditCategoryDialogFragment editCategoryDialogFragment = EditCategoryDialogFragment.newInstance(
                (_category_id, _text) -> mShowCategoryPresenter.changeCategory(_category_id, _text), category_id, text);
        editCategoryDialogFragment.show(getActivity().getSupportFragmentManager(),EditCategoryDialogFragment.TAG);
    }

    private void showPromptDelete(int category_id, String text) {
        AlertDialog.Builder ab = new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
            .setTitle(R.string.delete)
            .setMessage(MyApplication.INSTANCE.getString(R.string.ask_delete_cat, text))
            .setCancelable(false)
            .setPositiveButton(R.string.ok, (dialog, which) -> mShowCategoryPresenter.deleteCategoryById(category_id))
            .setNegativeButton(R.string.cancel, (dialog, which) -> { });
        AlertDialog dialog = ab.create();
        dialog.show();
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void showMessage(String message) {
        Timber.d(message);
        Snackbar.make(fabAddCategory, message, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void showMessage(int res_message) {
        showMessage(getString(res_message));
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void updateList(List<CategoryWithCount> categories) {
        adapter.clearAll();
        for (CategoryWithCount item: categories) {
            adapter.add(item);
        }
    }
}
