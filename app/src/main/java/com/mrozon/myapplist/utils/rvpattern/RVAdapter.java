package com.mrozon.myapplist.utils.rvpattern;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public abstract class RVAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    implements IBaseListAdapter<IBaseListItem>{

    protected ArrayList<IBaseListItem> items = new ArrayList<>();
    protected RVAdapterItemClickListener listener;

    public interface RVAdapterItemClickListener {
        void onItemClick(IBaseListItem item);
        void onItemLongClick(IBaseListItem item);
    }

    public RVAdapter() {

    }

    public void setListener(RVAdapterItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getLayoutId();
    }

    protected View inflateByViewType(Context context, int viewType, ViewGroup parent){
        return LayoutInflater.from(context).inflate(viewType, parent, false);
    }

    @Override
    public void add(IBaseListItem newItem) {
        items.add(newItem);
        notifyDataSetChanged();
    }

    @Override
    public void add(List<IBaseListItem> newItems) {
        for(IBaseListItem newItem: newItems){
            items.add(newItem);
            notifyDataSetChanged();
        }
    }

    @Override
    public void addAtPosition(int pos, IBaseListItem newItem) {
        items.add(pos, newItem);
        notifyDataSetChanged();
    }

    @Override
    public void remove(int position) {
        items.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public void clearAll() {
        items.clear();
        notifyDataSetChanged();
    }
}
