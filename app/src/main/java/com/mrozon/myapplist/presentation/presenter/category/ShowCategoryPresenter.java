package com.mrozon.myapplist.presentation.presenter.category;

import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.MyAppListDatabase;
import com.mrozon.myapplist.db.model.Category;
import com.mrozon.myapplist.db.model.CategoryWithCount;
import com.mrozon.myapplist.presentation.view.category.ShowCategoryView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;
import java.util.Observable;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class ShowCategoryPresenter extends MvpPresenter<ShowCategoryView> {

    public void getItems() {
        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
        final Disposable items = db.myAppListStore().selectCategoryWithCount()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__-> getViewState().showProgress())
//                .doOnTerminate(()->getViewState().hideProgress())
//                .doFinally(()->getViewState().hideProgress())
                .subscribe((List<CategoryWithCount> categories) -> {
                    getViewState().updateList(categories);
                    getViewState().hideProgress();
                    getViewState().showMessage("Ok");
                }, throwable -> {
                    getViewState().hideProgress();
                    getViewState().showMessage(throwable.getMessage());
                });
    }

    public void deleteCategoryById(int category_id) {
        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
        final Disposable items = db.myAppListStore().deleteByUserId(category_id)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__-> getViewState().showProgress())
//                .doOnTerminate(()->getViewState().hideProgress())
//                .doFinally(()->getViewState().hideProgress())
                .subscribe(__ -> {
                    getViewState().hideProgress();
                    getViewState().showMessage(R.string.category_deleted);
                }, throwable -> {
                    getViewState().hideProgress();
                    getViewState().showMessage(throwable.getMessage());
                });
    }

    public void addCategory(String text) {
        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
        Disposable disposable = Completable.fromAction(() -> db.myAppListStore().insertCategory(new Category(0,text)))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe(() -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(R.string.category_added);
                        },
                        (throwable -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
    }

    public void changeCategory(int category_id, String text) {
        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
        Disposable disposable = Completable.fromAction(() -> db.myAppListStore().updateCategory(new Category(category_id,text)))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe(() -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(R.string.ok);
                        },
                        (throwable -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
    }
}
