package com.mrozon.myapplist.db.model;

import android.graphics.drawable.Drawable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.BlobTransmogrifier;
import com.mrozon.myapplist.utils.rvpattern.IBaseListItem;

import static androidx.room.ForeignKey.CASCADE;
import static androidx.room.ForeignKey.SET_DEFAULT;

@Entity(foreignKeys =
        {@ForeignKey(entity = Category.class,
                parentColumns = "category_id",
                childColumns = "app_category",
                onDelete = SET_DEFAULT )},
        indices = {
                @Index(name = "app_category_index", value = {"app_category"})
        },tableName = "myapps")
@TypeConverters({BlobTransmogrifier.class})
public class App implements IBaseListItem {
    @PrimaryKey(autoGenerate=true)
    @ColumnInfo(name="app_id")
    public final int id;
    @ColumnInfo(name="app_title")
    public final String title;
    @ColumnInfo(name="app_package")
    public final String fullPackage;
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB, name = "app_logo")
    public final transient Drawable img;
    @ColumnInfo(name="app_favorite")
    public Boolean favorite;
    @ColumnInfo(name="app_comment")
    public String comment;
    @ColumnInfo(name="app_category", defaultValue = "1")
    public int fk_category;

    @Ignore
    public String categoryTitle;

    @Ignore
    public boolean isExisted;

    @Ignore
    public boolean isNew;

    public App(int id, String title, String fullPackage, Drawable img, Boolean favorite, String comment, int fk_category) {
        this.id = id;
        this.title = title;
        this.fullPackage = fullPackage;
        this.img = img;
        this.favorite = favorite;
        this.comment = comment;
        this.fk_category = fk_category;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_app;
    }
}
