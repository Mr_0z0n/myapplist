package com.mrozon.myapplist.repositories;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.model.App;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public class AppsRepository implements IAppsRepository {
    private static AppsRepository INSTANCE;

    public static AppsRepository getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AppsRepository();
        }
        return INSTANCE;
    }

    private boolean isSystemPackage(ApplicationInfo packageInfo) {
        return ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
    }

    private Drawable getIconApp(ApplicationInfo app){
        Context context = MyApplication.INSTANCE;
        try {
            return context.getPackageManager().getApplicationIcon(app.packageName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public  Observable<App> getNonSystemApps(){
        Context context = MyApplication.INSTANCE;
        PackageManager pm = context.getPackageManager();

        List<ApplicationInfo> apps = context.getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);
        return Observable.fromIterable(apps)
                .filter(app->!isSystemPackage(app))
                .map(applicationInfo -> {
                    ApplicationInfo ai = pm.getApplicationInfo( applicationInfo.packageName, 0);
                    return new App(0,
                        pm.getApplicationLabel(ai).toString(),
                        applicationInfo.packageName,
                        pm.getApplicationIcon(applicationInfo.packageName),
                        false,
                        "",
                        1);
                });
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Flowable<List<App>> getNonSystemApps2(){
        Context context = MyApplication.INSTANCE;
        PackageManager pm = context.getPackageManager();

        List<ApplicationInfo> apps = context.getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);
        return Flowable.fromIterable(apps)
                .filter(app->!isSystemPackage(app))
                .map(applicationInfo -> {
                    ApplicationInfo ai = pm.getApplicationInfo( applicationInfo.packageName, 0);
                    return new App(0,
                            pm.getApplicationLabel(ai).toString(),
                            applicationInfo.packageName,
                            pm.getApplicationIcon(applicationInfo.packageName),
//                            context.getDrawable(R.drawable.ic_add_user),
                            false,
                            "",
                            1);
                })
                .toList()
                .toFlowable();
    }

    @Override
    public Drawable getLogoApp(String packageName) {
        return null;
    }

}
