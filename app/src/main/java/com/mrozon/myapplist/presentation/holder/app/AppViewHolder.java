package com.mrozon.myapplist.presentation.holder.app;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mrozon.myapplist.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.app_image_delete)
    public ImageView app_image_delete;

    @BindView(R.id.app_image)
    public ImageView app_image;

    @BindView(R.id.app_image_share)
    public ImageView app_image_share;

    @BindView(R.id.app_image_comments)
    public ImageView app_image_comments;

    @BindView(R.id.app_image_favorite)
    public ImageView app_image_favorite;

    @BindView(R.id.app_image_download)
    public ImageView app_image_download;

    @BindView(R.id.app_label)
    public TextView app_label;

    @BindView(R.id.app_package)
    public TextView app_package;

    @BindView(R.id.app_category)
    public TextView app_category;

    public AppViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
