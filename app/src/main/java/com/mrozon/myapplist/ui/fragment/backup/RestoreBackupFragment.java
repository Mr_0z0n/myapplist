package com.mrozon.myapplist.ui.fragment.backup;

import android.Manifest;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.github.florent37.runtimepermission.PermissionResult;
import com.github.florent37.runtimepermission.rx.RxPermissions;
import com.google.android.material.snackbar.Snackbar;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.presentation.view.backup.RestoreBackupView;
import com.mrozon.myapplist.presentation.presenter.backup.RestoreBackupPresenter;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

public class RestoreBackupFragment extends MvpAppCompatFragment implements RestoreBackupView, View.OnClickListener {

    public static final String TAG = "RestoreBackupFragment";

    @BindView(R.id.restore_backup_local)
    Button restore_backup_local;

    @BindView(R.id.restore_backup_cloud)
    Button restore_backup_cloud;

    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;

    @InjectPresenter
    RestoreBackupPresenter mRestoreBackupPresenter;

    public static RestoreBackupFragment newInstance() {
        RestoreBackupFragment fragment = new RestoreBackupFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restore_backup, container, false);
        ButterKnife.bind(this, view);

        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity()))
                .getSupportActionBar()).setTitle(R.string.nav_backup_restore);

        restore_backup_local.setOnClickListener(this);
        restore_backup_cloud.setOnClickListener(this);


        return view;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        Disposable subscribe = new RxPermissions(getActivity()).request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(result -> {
                    int id = view.getId();
                    switch (id) {
                        case R.id.restore_backup_local:
                            mRestoreBackupPresenter.restoreLocalBackup(getContext());
                            break;
                        case R.id.restore_backup_cloud:
                            mRestoreBackupPresenter.restoreCloudBackup();
                            break;
                    }
                }, throwable -> {
                    final PermissionResult result = ((RxPermissions.Error) throwable).getResult();
                    if(result.hasDenied()) {
                        //permission denied, but you can ask again, eg:
                        new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                                .setMessage(R.string.accept_permissions_save_backup)
                                .setPositiveButton(R.string.yes, (dialog, which) -> {
                                    result.askAgain();
                                }) // ask again
                                .setNegativeButton(R.string.no, (dialog, which) -> {
                                    dialog.dismiss();
                                })
                                .show();
                    }
                    if(result.hasForeverDenied()) {
                        // you need to open setting manually if you really need it
                        result.goToSettings();
                    }
                });
    }

    @Override
    public void showMessage(int res_string) {
        showMessage(getString(res_string));
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(Objects.requireNonNull(getView()), message, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void showProgress() {
        progressBar2.setVisibility(View.VISIBLE);
        restore_backup_local.setEnabled(false);
        restore_backup_cloud.setEnabled(false);
    }

    @Override
    public void hideProgress() {
        progressBar2.setVisibility(View.GONE);
        restore_backup_local.setEnabled(true);
        restore_backup_cloud.setEnabled(true);
    }
}
