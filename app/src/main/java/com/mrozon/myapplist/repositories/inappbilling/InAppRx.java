package com.mrozon.myapplist.repositories.inappbilling;


import androidx.annotation.NonNull;

import org.solovyev.android.checkout.ActivityCheckout;
import org.solovyev.android.checkout.BillingRequests;
import org.solovyev.android.checkout.Checkout;
import org.solovyev.android.checkout.Inventory;
import org.solovyev.android.checkout.Purchase;
import org.solovyev.android.checkout.RequestListener;
import org.solovyev.android.checkout.Sku;

import javax.annotation.Nonnull;

import io.reactivex.Single;

public class InAppRx {

    @NonNull
    static Single<Inventory.Products> loadAllPurchases(@NonNull ActivityCheckout mCheckout,
                                                       @NonNull String typeSkus,
                                                       @NonNull String[] skus) {
        return Single.create(emitter -> {
            Inventory mInventory = mCheckout.makeInventory();
            mInventory.load(
                    Inventory.Request.create().loadAllPurchases().loadSkus(typeSkus,skus),
                    emitter::onSuccess);
        });
    }

    @NonNull
    static Single<Purchase> purchase(@NonNull ActivityCheckout mCheckout,
                                     @NonNull Sku sku) {
        return Single.create(emitter -> {
            mCheckout.startPurchaseFlow(sku, null, new RequestListener<Purchase>() {
                @Override
                public void onSuccess(@Nonnull Purchase result) {
                    emitter.onSuccess(result);
                }

                @Override
                public void onError(int response, @Nonnull Exception e) {
                    emitter.onError(e);
                }
            });
        });
    }

    @NonNull
    static Single<BillingRequests> getRequests(@NonNull ActivityCheckout mCheckout) {
        return Single.create(emitter -> {
            mCheckout.whenReady(new Checkout.EmptyListener() {
                @Override
                public void onReady(@Nonnull BillingRequests requests) {
                    emitter.onSuccess(requests);
                }

            });
        });
    }

    @NonNull
    static Single<Object> consume(@NonNull BillingRequests requests,
                                  @NonNull Purchase purchase) {
        return Single.create(emitter -> requests.consume(purchase.token, new RequestListener<Object>() {
            @Override
            public void onSuccess(@Nonnull Object result) {
                emitter.onSuccess(result);
            }

            @Override
            public void onError(int response, @Nonnull Exception e) {
                emitter.onError(e);
            }
        }));

    }
}
