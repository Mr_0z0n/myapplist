package com.mrozon.myapplist.repositories.inappbilling;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import org.solovyev.android.checkout.Inventory;
import org.solovyev.android.checkout.Purchase;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface InAppView extends MvpView {

    void loadedSkus(Inventory.Product product);
    void showMessage(String text);
    void purchased(Purchase purchase);
    void consumed(Purchase purchase);

}
