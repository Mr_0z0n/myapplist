package com.mrozon.myapplist.utils;

public final class AppConstants {

    public static final String TIMESTAMP_FORMAT = "dd MMM HH:mm";//"dd-MM-yyyy HH:mm:ss";
    public static final String TIMESTAMP_FILE_FORMAT = "dd_MM_yyyy_HH_mm";//"yyyyMMdd_HHmmss";
    public static final String TIMESTAMP_LABEL_FORMAT = "HH:mm dd-MM-yyyy";
    public static final String regexPersonName="^[a-zа-яё0-9_-]{1,30}$";
    public static final String APP_MARKET_LINK = "market://details?id=";
    public static final String APP_GOOGLE_PLAY_STORE_LINK = "https://play.google.com/store/apps/details?id=";
    public static int countItemsPerNativeAd = 6;
    public static final int INTERVAL_DAYS_FOR_RATE = 5;

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}