package com.mrozon.myapplist.utils.rvpattern;

public interface IBaseListItem {
    int getLayoutId();
}
