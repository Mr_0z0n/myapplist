package com.mrozon.myapplist.ui.activity.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ShareCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.presentation.view.home.HomeView;
import com.mrozon.myapplist.presentation.presenter.home.HomePresenter;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.mrozon.myapplist.repositories.inappbilling.InAppPresenter;
import com.mrozon.myapplist.repositories.inappbilling.InAppView;
import com.mrozon.myapplist.ui.fragment.rate.RateDialogFragment;
import com.mrozon.myapplist.utils.CommonUtils;
import com.squareup.picasso.Picasso;
import com.yandex.mobile.ads.AdView;

import org.solovyev.android.checkout.Inventory;
import org.solovyev.android.checkout.Purchase;
import org.solovyev.android.checkout.Sku;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import timber.log.Timber;

import static com.mrozon.myapplist.repositories.inappbilling.InAppPresenter.SKU_DONATE;
import static com.mrozon.myapplist.repositories.inappbilling.InAppPresenter.SKU_NO_ADS;
import static com.mrozon.myapplist.utils.AppConstants.INTERVAL_DAYS_FOR_RATE;
import static com.mrozon.myapplist.utils.AppUtils.openPlayStoreForApp;
import static com.mrozon.myapplist.utils.AppUtils.openPrivacyPolicyForApp;

public class HomeActivity extends MvpAppCompatActivity implements HomeView, InAppView,
        NavigationView.OnNavigationItemSelectedListener {

    public static final int RC_SIGN_IN = 9001;
    public static final String TAG = "HomeActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.banner_view)
    AdView mAdView;
    @BindView(R.id.contentFrame)
    FrameLayout frameLayout;

    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;

    @InjectPresenter
    HomePresenter mHomePresenter;
    @InjectPresenter
    InAppPresenter inAppPresenter;

    private Navigator navigator;
    private NavigatorHolder navigatorHolder;

    private Boolean ads_free;
    private Boolean donate;

    private RateDialogFragment rateDialogFragment;

    public static Intent getIntent(final Context context) {
        Intent intent = new Intent(context, HomeActivity.class);

        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        initActionBar();
        initFirebaseGoogleSignIn();
        initNavigationView();
        initCicerone();
        initInAppPurchase();

        if(getSupportFragmentManager().getFragments().isEmpty())
            mHomePresenter.onFirstShowFragment();

        showRateDialog();

    }

    private void showRateDialog(){
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        boolean rate_dialog = sharedPref.getBoolean("rate_dialog", true);
        if(!rate_dialog)
            return;

        Calendar current_date = Calendar.getInstance();
        current_date.add(Calendar.DATE,INTERVAL_DAYS_FOR_RATE);

        boolean rate_dialog_first_show = sharedPref.getBoolean("rate_dialog_first_show", true);
        if(rate_dialog_first_show){
            sharedPref.edit().putLong("rate_dialog_late", current_date.getTimeInMillis()).apply();
            sharedPref.edit().putBoolean("rate_dialog_first_show", false).apply();
            return;
        }


        long rate_dialog_late = sharedPref.getLong("rate_dialog_late", current_date.getTimeInMillis());
        if(Calendar.getInstance().getTimeInMillis()<rate_dialog_late)
            return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this,
                R.style.Theme_AppCompat_Light_Dialog_Alert)
                .setTitle(R.string.rate_dialog_title)
                .setIcon(R.drawable.ic_thumb_up_black_24dp)
                .setMessage(R.string.rate_dialog_message)
                .setCancelable(false)
                .setPositiveButton(R.string.rate_now, (dialog1, which) -> {
                    sharedPref.edit().putBoolean("rate_dialog", false).apply();
                    openPlayStoreForApp();
                })
                .setNeutralButton(R.string.rate_late, (dialog2, which) -> {
                    sharedPref.edit().putLong("rate_dialog_late", current_date.getTimeInMillis()).apply();
                })
                .setNegativeButton(R.string.no_thanks, (dialog3, which) -> {
                    sharedPref.edit().putBoolean("rate_dialog", false).apply();
                });
        builder.show();
    }



    private void initInAppPurchase() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        ads_free = sharedPref.getBoolean(SKU_NO_ADS, false);
        donate = sharedPref.getBoolean(SKU_DONATE, false);
        navigationView.getMenu().findItem(R.id.nav_no_ads).setVisible(!ads_free);
        navigationView.getMenu().findItem(R.id.nav_donate).setVisible(!donate);
        inAppPresenter.initializeInApp(this);
    }

    private void initCicerone() {
        navigator = new SupportAppNavigator(this, getSupportFragmentManager(), R.id.contentFrame);
        navigatorHolder = MyApplication.INSTANCE.getNavigatorHolder();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        inAppPresenter.getCheckout().onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            mHomePresenter.getResultGoogleSignIn(GoogleSignIn.getSignedInAccountFromIntent(data), mAuth);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and updateIndicatorType UI accordingly.
        updateUserInfo(mAuth.getCurrentUser());
    }

    @Override
    protected void onDestroy() {
        inAppPresenter.stop();
//        if(rateDialogFragment.isVisible())
//            rateDialogFragment.dismiss();
        super.onDestroy();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    private void initNavigationView() {
        navigationView.setNavigationItemSelectedListener(this);
        View headerLayout = navigationView.getHeaderView(0);
        TextView textVersion = headerLayout.findViewById(R.id.textVersion);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            textVersion.setText(getString(R.string.nav_header_version,version));
        } catch (PackageManager.NameNotFoundException e) {
            Timber.e(e,"getPackageInfo");
        }
        ImageView imageSign = headerLayout.findViewById(R.id.buttonSign);
        imageSign.setOnClickListener(v -> mHomePresenter.sign(HomeActivity.this, mGoogleSignInClient,mAuth));
    }

    private void initFirebaseGoogleSignIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(HomeActivity.this, gso);
        mAuth = FirebaseAuth.getInstance();
    }

    private void initActionBar() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case R.id.nav_cats:
                mHomePresenter.onShowCategories();
                break;

            case R.id.nav_apps:
                mHomePresenter.onShowApps();
                break;

            case R.id.nav_backup_save:
                mHomePresenter.onShowSaveBackup();
                break;

            case R.id.nav_backup_restore:
                mHomePresenter.onShowRestoreBackup();
                break;

            case R.id.nav_rate:
                openPlayStoreForApp();
                break;

            case R.id.nav_send_email:
                ShareCompat.IntentBuilder.from(this)
                        .setType("message/rfc822")
                        .addEmailTo("mr.ozon1982@gmail.com")
                        .setSubject(getString(R.string.app_name))
                        .setText(getString(R.string.text_email))
                        //.setHtmlText(body) //If you are using HTML in your body text
                        .setChooserTitle(getString(R.string.chooser_email))
                        .startChooser();
                break;

            case R.id.nav_no_ads:
//                inAppPresenter.purchaseSku(SKU_NO_ADS);
                break;

            case R.id.nav_donate:
//                inAppPresenter.purchaseSku(SKU_DONATE);
                break;

            case R.id.nav_privacy_policy:
                openPrivacyPolicyForApp(this);
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void closeDrawer() {
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void updateUserInfo(FirebaseUser user) {
        View headerLayout = navigationView.getHeaderView(0);
        ImageView imageSign = headerLayout.findViewById(R.id.buttonSign);
        ImageView imagePhoto = headerLayout.findViewById(R.id.imageView);
        TextView tvUserName = headerLayout.findViewById(R.id.textUserName);
        TextView tvEmail = headerLayout.findViewById(R.id.textEmail);
        if(user==null){
            tvUserName.setText(R.string.nav_header_title);
            tvEmail.setText(R.string.nav_header_subtitle);
            imageSign.setImageResource(R.drawable.ic_login);
            imagePhoto.setImageResource(R.drawable.ic_add_user);
        }
        else
        {
            tvUserName.setText(user.getDisplayName());
            tvEmail.setText(user.getEmail());
            imageSign.setImageResource(R.drawable.ic_logout);
            Uri photoUrl = user.getPhotoUrl();
            if (photoUrl == null) {
                imagePhoto.setImageResource(R.drawable.ic_no_photo);
            }
            else
            {
                showPhoto(imagePhoto,0,photoUrl);
            }
        }
    }

    private void showPhoto(ImageView imagePhoto, int res, Uri uri){
        if(res>0){
            Picasso.with(HomeActivity.this)
                    .load(res)
                    .resize(100, 100)
                    .centerCrop()
                    .transform(new CommonUtils.CircleTransform())
                    .into(imagePhoto);
        }
        else
        {
            Picasso.with(HomeActivity.this)
                    .load(uri)
                    .resize(100, 100)
                    .centerCrop()
                    .transform(new CommonUtils.CircleTransform())
                    .into(imagePhoto);
        }
    }

    @Override
    public void showError(int mes_id) {
        showErrorMessage(getString(mes_id));
    }

    @Override
    public void showError(String message) {
        showErrorMessage(message);
    }

    @Override
    public void showGreating(String message) {
        showInfoMessage(message);
    }

    private void showInfoMessage(String message){
        Snackbar.make(getWindow().getDecorView().getRootView(), message, Snackbar.LENGTH_LONG)
                .show();
    }

    private void showErrorMessage(String message){
//        Snackbar.make(getWindow().getDecorView().getRootView(), message, Snackbar.LENGTH_LONG)
//                .show();
        AlertDialog.Builder ab = new AlertDialog.Builder(this)
                .setTitle(R.string.error)
                .setIcon(R.drawable.ic_error_outline_black_24dp)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(R.string.ok, (dialog, which) -> {});
        AlertDialog dialog = ab.create();
        dialog.show();
    }

    @Override
    public void loadedSkus(Inventory.Product product) {
        if(product.getSkus().isEmpty()){
            navigationView.getMenu().findItem(R.id.nav_no_ads).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_donate).setVisible(false);
            return;
        }
        for (Sku sku:product.getSkus()) {

            if(product.isPurchased(sku)) {
                if (sku.id.code.equals(SKU_NO_ADS) && !ads_free) {
                    setPurchased(SKU_NO_ADS, true);
                }
                if (sku.id.code.equals(SKU_DONATE) && !donate) {
                    setPurchased(SKU_DONATE, true);
                }
            }
            else {
                if (sku.id.code.equals(SKU_NO_ADS) && ads_free) {
                    setPurchased(SKU_NO_ADS, false);
                }
                if (sku.id.code.equals(SKU_DONATE) && donate) {
                    setPurchased(SKU_DONATE, false);
                }
            }
        }
    }

    private void setPurchased(String sku, Boolean value){
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        switch (sku){
            case SKU_NO_ADS:
                ads_free = value;
                navigationView.getMenu().findItem(R.id.nav_no_ads).setVisible(!value);
                sharedPref.edit().putBoolean(SKU_NO_ADS,value).apply();
//                if(value) {
//                    frameLayout.setPadding(0, 0, 0, 0);
//                    if (mAdView != null)
//                        mAdView.destroy();
//                }
//                else {
//                    frameLayout.setPadding(0, 0, 0, 50);
//                    mHomePresenter.addAdView(mAdView);
//                }
                break;
            case SKU_DONATE:
                donate = value;
                navigationView.getMenu().findItem(R.id.nav_donate).setVisible(!value);
                sharedPref.edit().putBoolean(SKU_DONATE,value).apply();
                break;
        }
    }

    @Override
    public void showMessage(String text) {
        showInfoMessage(text);
    }

    @Override
    public void purchased(Purchase purchase) {
        if(purchase.state == Purchase.State.PURCHASED){
            switch (purchase.sku){
                case SKU_NO_ADS:
                    setPurchased(SKU_NO_ADS, true);
                    showMessage(getString(R.string.no_ads_purchased));
                    break;
                case SKU_DONATE:
                    setPurchased(SKU_DONATE, true);
                    showMessage(getString(R.string.thanks_for_donate));
                    break;
            }
        }
        inAppPresenter.reloadInventory();
    }

    @Override
    public void consumed(Purchase purchase) {
        showMessage(String.valueOf(purchase.state));
        inAppPresenter.reloadInventory();
    }
}
