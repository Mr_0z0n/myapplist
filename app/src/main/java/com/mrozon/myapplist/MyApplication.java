package com.mrozon.myapplist;

import android.app.Application;
import android.content.Context;
import android.view.Menu;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;

import com.google.firebase.FirebaseApp;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import org.solovyev.android.checkout.Billing;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import timber.log.Timber;

/**
 * Created by ozon on 07.10.17.
 */

public class MyApplication extends Application {

    public static MyApplication INSTANCE;
    private Cicerone<Router> cicerone;

    private static final String API_KEY = "13870be0-885b-4284-8326-401f6fbaccc5";
    private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkcirruZb8SCL4/AUcKjeA92N1wrqNiT6QPrnQkR0yaGRlK7TiTMuiHqEh9bRd94m2zCVZYZaBMDrMPhLqCKwFLs+jhZFiAbF3Ua15T8Q6bpieYiB9xGUo6fcmD/R5acVBf5sL9n8x3TYJRatNpyNhH9p4Vh9Uj+I6eZiYgUlVxMR/33JVgFJQ4RZxHGtQYxJVp4NTxHJsPPcfXT2Dj4yAmhZw2M8zG8jBML5BQBe9oZc0hwtq7gG7WHtBJ8t25/Gj5j5fqBIye714bPjFOHocUgIR6FkUBKXvnMBXBfSqMssgBTiC85fHsZGW6xyapTcDFlDkJu5R2GvuZh45zg0HwIDAQAB";

    private final Billing mBilling = new Billing(this, new Billing.DefaultConfiguration() {
        @NonNull
        @Override public String getPublicKey() {
            return BASE64_PUBLIC_KEY;
        }

    });

    public Billing getBilling() {
        return mBilling;
    }

    public void onCreate() {
        super.onCreate();
        INSTANCE = this;

        initCicerone();
        initYandexMetrica();
        initTimber();
        initCrashlytics();
    }

    private void initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void initCrashlytics() {
        FirebaseApp.initializeApp(this);
        if(BuildConfig.DEBUG) {
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(false);
        }else{
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        }
    }

    private void initYandexMetrica() {
        // Инициализация AppMetrica SDK
        YandexMetricaConfig yandexMetricaConfig = YandexMetricaConfig.newConfigBuilder(API_KEY)
                .build();
        YandexMetrica.activate(getApplicationContext(), yandexMetricaConfig);
        // Отслеживание активности пользователей
        YandexMetrica.enableActivityAutoTracking(this);
    }

    private void initCicerone() {
        cicerone = Cicerone.create();
    }

    public NavigatorHolder getNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

    public Router getRouter() {
        return cicerone.getRouter();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


}
