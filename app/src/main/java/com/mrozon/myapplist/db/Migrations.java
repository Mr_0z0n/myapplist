package com.mrozon.myapplist.db;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;
import androidx.room.OnConflictStrategy;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.text.ParseException;
import java.util.Date;

public class Migrations {

    static final Migration FROM_1_TO_2=new Migration(1,2) {

        @Override
        public void migrate(@NonNull SupportSQLiteDatabase db) {
            //create new tables
            db.execSQL("CREATE TABLE `myapps` (`app_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `app_title` TEXT, `app_package` TEXT, `app_logo` BLOB, `app_favorite` INTEGER, `app_comment` TEXT, `app_category` INTEGER NOT NULL DEFAULT 1, FOREIGN KEY(`app_category`) REFERENCES `mycategories`(`category_id`) ON UPDATE NO ACTION ON DELETE SET DEFAULT )");
            db.execSQL("CREATE INDEX `app_category_index` ON `myapps` (`app_category`)");
            db.execSQL("CREATE TABLE `mycategories` (`category_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `category_title` TEXT)");
            //copy data
            Cursor cursor = db.query("SELECT * FROM categories");
            while (cursor.moveToNext()){
                //get row
                ContentValues contentValues = new ContentValues();
                contentValues.put("category_id", cursor.getInt(cursor.getColumnIndex("_id")));
                contentValues.put("category_title", cursor.getString(cursor.getColumnIndex("title")));
                db.insert("mycategories", OnConflictStrategy.IGNORE, contentValues);
            }
            cursor.close();
            cursor = db.query("SELECT * FROM APPS");
            while (cursor.moveToNext()){
                //get rows
                ContentValues contentValues = new ContentValues();
//                contentValues.put("app_logo",0);
                contentValues.put("app_comment", cursor.getString(cursor.getColumnIndex("comments")));
                contentValues.put("app_title", cursor.getString(cursor.getColumnIndex("label")));
                contentValues.put("app_favorite", cursor.getInt(cursor.getColumnIndex("favorite")));
//                contentValues.put("app_id",0);
                contentValues.put("app_package", cursor.getString(cursor.getColumnIndex("package")));
                contentValues.put("app_category", cursor.getInt(cursor.getColumnIndex("category_id")));
                db.insert("myapps", OnConflictStrategy.IGNORE, contentValues);
            }
            cursor.close();
            //delete old tables
            db.execSQL("DROP TABLE IF EXISTS APPS");
            db.execSQL("DROP TABLE IF EXISTS categories");
            //create view
            db.execSQL("CREATE VIEW `CategoryWithCount` AS select category_id, category_title, count(*) as count from myapps, mycategories where app_category=category_id group by category_id UNION select category_id, category_title, 0 as count from mycategories LEFT OUTER JOIN myapps on app_category=category_id where app_id is NULL order by count desc, category_title asc");
            db.execSQL("CREATE VIEW `AppWithCategory` AS select myapps.*, mycategories.* from myapps, mycategories where app_category=category_id");
        }
    };


}
