package com.mrozon.myapplist.db.model;

import com.mrozon.myapplist.R;
import com.mrozon.myapplist.utils.rvpattern.IBaseListItem;
import com.yandex.mobile.ads.nativeads.NativeContentAd;

public class MyNativeContentAd implements IBaseListItem {

    private NativeContentAd nativeContentAd;

    public MyNativeContentAd(NativeContentAd nativeContentAd) {
        this.nativeContentAd = nativeContentAd;
    }

    public NativeContentAd getNativeContentAd() {
        return nativeContentAd;
    }

    @Override
    public int getLayoutId() {
        return R.layout.widget_native_template;
    }
}
