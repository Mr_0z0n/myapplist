package com.mrozon.myapplist.db.model;

import androidx.room.DatabaseView;

import com.mrozon.myapplist.R;
import com.mrozon.myapplist.utils.rvpattern.IBaseListItem;


@DatabaseView("select category_id, category_title, count(*) as count " +
        "from myapps, mycategories where app_category=category_id " +
        "group by category_id " +
        "UNION " +
        "select category_id, category_title, 0 as count " +
        "from mycategories LEFT OUTER JOIN myapps on app_category=category_id " +
        "where app_id is NULL " +
        "order by count desc, category_title asc")
public class CategoryWithCount implements IBaseListItem {

    public int category_id;
    public String category_title;
    public int count;

    @Override
    public int getLayoutId() {
        return R.layout.item_category;
    }
}
