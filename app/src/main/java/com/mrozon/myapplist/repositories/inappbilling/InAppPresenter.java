package com.mrozon.myapplist.repositories.inappbilling;

import android.app.Activity;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mrozon.myapplist.MyApplication;

import org.solovyev.android.checkout.ActivityCheckout;
import org.solovyev.android.checkout.Checkout;
import org.solovyev.android.checkout.Inventory;
import org.solovyev.android.checkout.ProductTypes;
import org.solovyev.android.checkout.Purchase;
import org.solovyev.android.checkout.Sku;

import io.reactivex.disposables.Disposable;

@InjectViewState
public class InAppPresenter extends MvpPresenter<InAppView> {

    public static final String SKU_NO_ADS = "no_ads";
    public static final String SKU_DONATE = "donate";

    private ActivityCheckout mCheckout;
    private String[] skus = {SKU_NO_ADS, SKU_DONATE};
    private Inventory.Product product;

    public void initializeInApp(Activity activity){
        mCheckout = Checkout.forActivity(activity, MyApplication.INSTANCE.getBilling());
        mCheckout.start();
        Disposable subscribe = InAppRx.loadAllPurchases(mCheckout, ProductTypes.IN_APP, skus)
//                .subscribeOn(Schedulers.computation())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe((products) -> {
                            product = products.get(ProductTypes.IN_APP);
                            getViewState().loadedSkus(product);
                        },
                        (throwable -> {
                        }));

    }

    public void stop(){
        if(mCheckout!=null)
            mCheckout.stop();
    }

    public void reloadInventory(){
        Disposable subscribe = InAppRx.loadAllPurchases(mCheckout, ProductTypes.IN_APP, skus)
                .subscribe((products) -> {
                            product = products.get(ProductTypes.IN_APP);
                            getViewState().loadedSkus(product);
                        },
                        (throwable -> {
                        }));
    }

    public ActivityCheckout getCheckout() {
        return mCheckout;
    }

    public void purchaseSku(Sku sku){
        Purchase purchase = product.getPurchaseInState(sku, Purchase.State.PURCHASED);
        if (purchase != null) {
            consume(purchase);
        } else {
            purchase(sku);
        }
    }

    public void purchaseSku(String _sku){
        Sku sku = product.getSku(_sku);
        if(sku!=null)
            purchaseSku(sku);
    }

    private void consume(Purchase purchase) {
        Disposable subscribe = InAppRx.getRequests(mCheckout)
                .flatMap(requests -> InAppRx.consume(requests, purchase))
//                .subscribeOn(Schedulers.computation())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe((Object object) -> {
//                            getViewState().hideProgress();
                            getViewState().consumed(purchase);
                        },
                        (throwable -> {
//                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
        ;
    }

    private void purchase(Sku sku) {
        Disposable subscribe = InAppRx.purchase(mCheckout, sku)
//                .subscribeOn(Schedulers.computation())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe((Purchase purchase) -> {
//                            getViewState().hideProgress();
                            getViewState().purchased(purchase);
                        },
                        (throwable -> {
//                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
    }


}
