package com.mrozon.myapplist.ui.fragment.app;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.github.florent37.runtimepermission.PermissionResult;
import com.github.florent37.runtimepermission.rx.RxPermissions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.model.App;
import com.mrozon.myapplist.db.model.Category;
import com.mrozon.myapplist.db.model.CategoryWithCount;
import com.mrozon.myapplist.db.model.MyNativeContentAd;
import com.mrozon.myapplist.presentation.adapter.app.AppAdapter;
import com.mrozon.myapplist.presentation.view.app.ShowAppView;
import com.mrozon.myapplist.presentation.presenter.app.ShowAppPresenter;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.mrozon.myapplist.utils.rvpattern.IBaseListItem;
import com.yandex.mobile.ads.nativeads.NativeContentAd;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

import static com.mrozon.myapplist.repositories.inappbilling.InAppPresenter.SKU_NO_ADS;
import static com.mrozon.myapplist.utils.AppConstants.countItemsPerNativeAd;
import static com.mrozon.myapplist.utils.AppUtils.openPlayStoreForApp;
import static com.mrozon.myapplist.utils.AppUtils.shareApp;

public class ShowAppFragment extends MvpAppCompatFragment implements ShowAppView {

    public static final String TAG = "ShowAppFragment";

    @BindView(R.id.rvItems)
    RecyclerView rvItems;

    @BindView(R.id.fabFilterApps)
    FloatingActionButton fabFilterApps;

    @BindView(R.id.srlItems)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.etSearchApp)
    EditText etSearchApp;

    @InjectPresenter
    ShowAppPresenter mShowAppPresenter;

    private AppAdapter adapter;
    private static Menu menu;



    public static ShowAppFragment newInstance() {
        ShowAppFragment fragment = new ShowAppFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_app, container, false);
        ButterKnife.bind(this, view);

//        setRetainInstance(true);
        setHasOptionsMenu(true);

        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity()))
                .getSupportActionBar()).setTitle(R.string.app_main);

        adapter = new AppAdapter(new AppAdapter.ClickListener() {
            @Override
            public void onLogoClick(App app) {
                openPlayStoreForApp(app.fullPackage);
            }

            @Override
            public void onFavoriteClick(App app) {
                mShowAppPresenter.changeFavorite(app);
            }

            @Override
            public void onShareClick(App app) {
                shareApp(app);
            }

            @Override
            public void onCommentClick(App app) {
                if(app.comment.isEmpty()){
                    showDialog(app,"");
                }
                else
                {
                    AlertDialog.Builder ab = new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                            .setMessage(app.comment)
                            .setCancelable(true)
                            .setPositiveButton(R.string.ok, (dialog, which) -> {})
                            .setNegativeButton(R.string.app_change, (dialog, which) -> { showDialog(app,app.comment);})
                            .setNeutralButton(R.string.app_delete_dialog, (dialog, which) -> {mShowAppPresenter.deleteComment(app);});
                    AlertDialog dialog = ab.create();
                    dialog.show();
                }
            }

            @Override
            public void onChooseCategory(App app) {
                ChooseCategoryDialogFragment chooseCategoryDialogFragment = ChooseCategoryDialogFragment.newInstance(
                        categoryTitle -> {
                            mShowAppPresenter.changeCategory(app, categoryTitle);
                        },
                        mShowAppPresenter.getCategories(), app.categoryTitle);
                chooseCategoryDialogFragment.show(getActivity().getSupportFragmentManager(),ChooseCategoryDialogFragment.TAG);

            }

            @Override
            public void onUninstallApp(App app) {
                Intent uninstallIntent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
                uninstallIntent.setData(Uri.parse("package:" + app.fullPackage));
                uninstallIntent.putExtra(Intent.EXTRA_RETURN_RESULT, true);
                Objects.requireNonNull(getActivity()).startActivity(uninstallIntent);
                showMessage(R.string.app_uninstalled);
                //TODO подумать над удалением и автообновлением списка
            }

            @Override
            public void onDownloadApp(App app) {
                Disposable subscribe = new RxPermissions(getActivity()).request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribe(result -> {
                            mShowAppPresenter.downloadApk(app);
                        }, throwable -> {
                            final PermissionResult result = ((RxPermissions.Error) throwable).getResult();
                            if(result.hasDenied()) {
                                //permission denied, but you can ask again, eg:
                                new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                                        .setMessage(R.string.accept_permissions)
                                        .setPositiveButton(R.string.yes, (dialog, which) -> {
                                            result.askAgain();
                                        }) // ask again
                                        .setNegativeButton(R.string.no, (dialog, which) -> {
                                            dialog.dismiss();
                                        })
                                        .show();
                            }
                            if(result.hasForeverDenied()) {
                                // you need to open setting manually if you really need it
                                result.goToSettings();
                            }
                        });
            }
        });
        LinearLayoutManager layout = new LinearLayoutManager(getContext());
        rvItems.setLayoutManager(layout);
        rvItems.setAdapter(adapter);

        fabFilterApps.setOnClickListener(l->{
            ChooseCategoryDialogFragment chooseCategoryDialogFragment = ChooseCategoryDialogFragment.newInstance(
                    categoryTitle -> {
//                        showMessage(categoryTitle);
                        mShowAppPresenter.setObservableCategoryTitle(categoryTitle);
                    },
                    mShowAppPresenter.getCategories(), "");
            chooseCategoryDialogFragment.show(getActivity().getSupportFragmentManager(),ChooseCategoryDialogFragment.TAG);

        });

        mShowAppPresenter.filterApps(etSearchApp);

        mShowAppPresenter.selectAllCategories();
        mShowAppPresenter.getItems();
        mSwipeRefreshLayout.setOnRefreshListener(()->mShowAppPresenter.getItems());

        mShowAppPresenter.createNativeAdLoader();

        return view;
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        ShowAppFragment.menu = menu;
//        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_favorite:
//                Timber.d("menu_favorite click");
                mShowAppPresenter.setObservableFavoriteMenu();
                break;
            case R.id.menu_install_app:
                mShowAppPresenter.setObservableInstallAppMenu();
                break;
        }
        return true;
    }

    private void showDialog(App app, String _comment) {
        EditAppDialogFragment editAppDialogFragment = EditAppDialogFragment.newInstance(comment -> {
            mShowAppPresenter.changeComment(app, comment);
        },_comment);
        editAppDialogFragment.show(getActivity().getSupportFragmentManager(),EditAppDialogFragment.TAG);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void showMessage(String message) {
        Timber.d(message);
        Snackbar.make(fabFilterApps, message, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void showMessage(int res_message) {
        showMessage(getString(res_message));
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void updateList(List<App> apps) {
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        boolean ads_free = sharedPref.getBoolean(SKU_NO_ADS, false);
        if(!ads_free) {
            int ads_capacity = apps.size() / countItemsPerNativeAd;
            if (apps.size() + ads_capacity / countItemsPerNativeAd > ads_capacity)
                ads_capacity = apps.size() + ads_capacity / countItemsPerNativeAd;
            mShowAppPresenter.loadAds(ads_capacity);
        }
        adapter.clearAll();
        for (App item: apps) {
            adapter.add(item);
        }
    }

    @Override
    public void changeFavoriteIcon(Boolean isFavorite) {
        if(menu!=null){
            MenuItem menuItem = menu.findItem(R.id.menu_favorite);
            if(menuItem!=null)
                menuItem.setIcon(isFavorite?R.drawable.ic_bookmark_red_900_48dp:R.drawable.ic_bookmark_border_red_900_48dp);
        }
    }


    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        mShowAppPresenter.menuReady();
    }

    @Override
    public void changeInstallAppIcon(Boolean onlyInstalledApps) {
        if(menu!=null){
            MenuItem menuItem = menu.findItem(R.id.menu_install_app);
            if(menuItem!=null)
                menuItem.setIcon(onlyInstalledApps?R.drawable.ic_apps_green_a700_48dp:R.drawable.ic_delete_forever_green_a700_48dp);
        }
    }

    @Override
    public void showMessageWithOpenAction(int res_message, String path) {
        Snackbar snackbar = Snackbar.make(fabFilterApps, res_message, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.show, l -> openFolder(path) );
        snackbar.show();
    }

    @Override
    public void loadAd(NativeContentAd nativeContentAd) {
        ArrayList<IBaseListItem> items = adapter.getAll();
        Timber.d("loadAd:  %s %s", adapter.getItemCount(), adapter.getItemCount() / countItemsPerNativeAd);
        for(int i=1; i<=adapter.getItemCount()/countItemsPerNativeAd;i++){
            if(i * countItemsPerNativeAd-1>=items.size())
                break;
            if (!(items.get(i * countItemsPerNativeAd-1) instanceof MyNativeContentAd)) {
                adapter.addAtPosition(i * countItemsPerNativeAd-1, new MyNativeContentAd(nativeContentAd));
                break;
            }
        }
    }

    private void sendApk(String path) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(path)));
        intent.setType("application/vnd.android.package-archive");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(Intent.createChooser(intent, getString(R.string.open_folder)));
    }

    private void openFolder(String path) {
        //Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.parse(path+ File.separator);
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(uri, "resource/folder");
        startActivity(Intent.createChooser(intent, getString(R.string.open_folder)));
    }

}
