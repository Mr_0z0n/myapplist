package com.mrozon.myapplist.ui.fragment.category;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.arellomobile.mvp.MvpAppCompatDialogFragment;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.presentation.adapter.category.CategoryAdapter;
import com.mrozon.myapplist.presentation.view.category.EditCategoryDialogView;
import com.mrozon.myapplist.presentation.presenter.category.EditCategoryDialogPresenter;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class EditCategoryDialogFragment extends MvpAppCompatDialogFragment implements EditCategoryDialogView {
    public static final String TAG = "EditCategoryDialogFragment";

    @InjectPresenter
    EditCategoryDialogPresenter mEditCategoryDialogPresenter;

    @BindView(R.id.edit_category_title)
    EditText edit_category_title;


    private static CategoryAdapter.ClickListener onPositiveClickListener;
    private Disposable subscribe;

    public static EditCategoryDialogFragment newInstance(CategoryAdapter.ClickListener onPositiveClickListener,
                                                         int category_id, String category_title) {
        EditCategoryDialogFragment fragment = new EditCategoryDialogFragment();

        Bundle args = new Bundle();
        args.putInt("category_id",category_id);
        args.putString("category_title",category_title);
        fragment.setArguments(args);

        EditCategoryDialogFragment.onPositiveClickListener = onPositiveClickListener;

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        int category_id = 0;
        String category_title = "";
        if(getArguments()!=null) {
            category_id = getArguments().getInt("category_id");
            category_title = getArguments().getString("category_title");
        }

        LayoutInflater factory = LayoutInflater.from(getContext());
        final View view = factory.inflate(R.layout.fragment_edit_category_dialog, null);
        ButterKnife.bind(this, view);

        edit_category_title.setText(category_title);

        int finalCategory_id = category_id;
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()), R.style.Theme_AppCompat_Light_Dialog_Alert)
                .setTitle(category_id==0?R.string.add_cat_title:R.string.change_cat_title)
                .setView(view)
                .setIcon(R.drawable.ic_subject_black_24dp)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, (dialog1, which) -> {
                    if(onPositiveClickListener!=null)
                        onPositiveClickListener.onClick(finalCategory_id,edit_category_title.getText().toString());
                })
                .setNegativeButton(R.string.cancel, (dialog12, which) -> {

                });

        AlertDialog dialog = builder.create();

        subscribe = RxTextView.textChanges(edit_category_title)
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(Schedulers.computation())
                .map(CharSequence::toString)
                .map(s -> mEditCategoryDialogPresenter.checkInputValue(s))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(error -> {
                    if (error.length() > 0)
                        edit_category_title.setError(error);
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(error.length() == 0);
                });


        return dialog;

    }

    @Override
    public void onDestroyView() {
        subscribe.dispose();
        super.onDestroyView();
    }
}
