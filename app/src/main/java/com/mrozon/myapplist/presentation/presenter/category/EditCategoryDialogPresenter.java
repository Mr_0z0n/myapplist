package com.mrozon.myapplist.presentation.presenter.category;

import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.MyAppListDatabase;
import com.mrozon.myapplist.db.model.Category;
import com.mrozon.myapplist.presentation.view.category.EditCategoryDialogView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.mrozon.myapplist.utils.AppConstants.regexPersonName;

@InjectViewState
public class EditCategoryDialogPresenter extends MvpPresenter<EditCategoryDialogView> {

    public String checkInputValue(String text){
        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
        if(text.length()<1)
            return MyApplication.INSTANCE.getString(R.string.empty_field);
        Pattern p = Pattern.compile(regexPersonName,Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        if(!m.matches())
            return MyApplication.INSTANCE.getString(R.string.incorrect_value);
        Category category = db.myAppListStore().findCategoryByTitle(text);
        if(category!=null)
            return MyApplication.INSTANCE.getString(R.string.cat_already_exists);
        return "";
    }

}
