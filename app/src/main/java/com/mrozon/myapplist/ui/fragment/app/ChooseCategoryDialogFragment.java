package com.mrozon.myapplist.ui.fragment.app;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.arellomobile.mvp.MvpAppCompatDialogFragment;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.model.Category;
import com.mrozon.myapplist.presentation.view.app.ChooseCategoryDialogView;
import com.mrozon.myapplist.presentation.presenter.app.ChooseCategoryDialogPresenter;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.ButterKnife;

public class ChooseCategoryDialogFragment extends MvpAppCompatDialogFragment implements ChooseCategoryDialogView {
    public static final String TAG = "ChooseCategoryDialogFragment";

    public interface SelectCategoryListener {
        void onSelected(String categoryTitle);
    }

    private static SelectCategoryListener selectCategoryListener;

    @InjectPresenter
    ChooseCategoryDialogPresenter mChooseCategoryDialogPresenter;

    public static ChooseCategoryDialogFragment newInstance(SelectCategoryListener selectCategoryListener,
                                                   ArrayList<String> categoryList, String title) {
        ChooseCategoryDialogFragment fragment = new ChooseCategoryDialogFragment();

        Bundle args = new Bundle();
        args.putString("title",title);
        args.putStringArrayList("categoryList", categoryList);
        fragment.setArguments(args);

        ChooseCategoryDialogFragment.selectCategoryListener = selectCategoryListener;

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        String title = "";
        ArrayList<String> categoryList = null;
        if(getArguments()!=null) {
            title = getArguments().getString("title");
            categoryList = getArguments().getStringArrayList("categoryList");
        }

        assert categoryList != null;
        String[] arr = new String[categoryList.size()];
        arr = categoryList.toArray(arr);

        ArrayList<String> finalCategoryList = categoryList;
        AlertDialog.Builder ab = new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                .setTitle(R.string.select_category)
                .setSingleChoiceItems(arr,categoryList.indexOf(title),null)
                .setCancelable(true)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    if(selectCategoryListener!=null) {
                        int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        if(selectedPosition==-1)
                            selectCategoryListener.onSelected("");
                        else
                            selectCategoryListener.onSelected(finalCategoryList.get(selectedPosition));
                    }
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> {});
        return ab.create();

    }
}
