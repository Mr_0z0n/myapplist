package com.mrozon.myapplist.db;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.room.TypeConverter;

import java.io.ByteArrayOutputStream;
import java.util.Date;

public class BlobTransmogrifier {
    @TypeConverter
    public static byte[] fromDrawable(Drawable drawable) {
        if (drawable == null) {
            return (null);
        }
//        Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
        Bitmap bitmap = getBitmapFromDrawable(drawable);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    private static Bitmap getBitmapFromDrawable(@NonNull Drawable drawable) {
        final Bitmap bmp = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bmp);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bmp;
    }

    @TypeConverter
    public static Drawable toDrawable(byte[] b) {
        if (b == null) {
            return (null);
        }
//        Resources resources = MyApplication.getInstance().getBaseContext().getResources();
//        return new BitmapDrawable(resources, BitmapFactory.decodeByteArray(b, 0, b.length));
        return new BitmapDrawable(BitmapFactory.decodeByteArray(b, 0, b.length));
    }

    @TypeConverter
    public static Long fromDate(Date date) {
        if (date==null) {
            return(null);
        }

        return(date.getTime());
    }

    @TypeConverter
    public static Date toDate(Long millisSinceEpoch) {
        if (millisSinceEpoch==null) {
            return(null);
        }

        return(new Date(millisSinceEpoch));
    }
}
