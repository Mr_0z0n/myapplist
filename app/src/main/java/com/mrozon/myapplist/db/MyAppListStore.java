package com.mrozon.myapplist.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.mrozon.myapplist.db.model.App;
import com.mrozon.myapplist.db.model.AppWithCategory;
import com.mrozon.myapplist.db.model.Category;
import com.mrozon.myapplist.db.model.CategoryWithCount;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface MyAppListStore {

    //categories
    @Query("SELECT * FROM mycategories")
    Flowable<List<Category>> selectCategories();

    @Query("DELETE FROM mycategories WHERE category_id = :category_id")
    Single<Integer> deleteByUserId(int category_id);

    @Query("SELECT * FROM mycategories where category_title=:title limit 1")
    Category findCategoryByTitle(String title);

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    void insertCategory(Category... category);

    @Update(onConflict= OnConflictStrategy.REPLACE)
    void updateCategory(Category... category);

    //categories-view
    @Query("SELECT * FROM CategoryWithCount")
    Flowable<List<CategoryWithCount>> selectCategoryWithCount();

    //apps
    @Query("SELECT * FROM myapps")
    Flowable<List<App>> selectApps();

    //apps-view
    @Query("select * from AppWithCategory")
    Flowable<List<AppWithCategory>> selectAppWithCategory();

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    void insertApp(App... app);

    @Update(onConflict= OnConflictStrategy.REPLACE)
    void updateApp(App... app);

}
