package com.mrozon.myapplist.ui.fragment.rate;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.MvpAppCompatDialogFragment;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.presentation.view.rate.RateDialogView;
import com.mrozon.myapplist.presentation.presenter.rate.RateDialogPresenter;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.Objects;

public class RateDialogFragment extends MvpAppCompatDialogFragment implements RateDialogView {
    public static final String TAG = "RateDialogFragment";

    @InjectPresenter
    RateDialogPresenter mRateDialogPresenter;

    public static RateDialogFragment newInstance() {
        RateDialogFragment fragment = new RateDialogFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()),
                R.style.Theme_AppCompat_Light_Dialog_Alert)
                .setTitle(R.string.rate_dialog_title)
                .setIcon(R.drawable.ic_thumb_up_black_24dp)
                .setMessage(R.string.rate_dialog_message)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, (dialog1, which) -> {

                })
                .setNegativeButton(R.string.cancel, (dialog12, which) -> {

                });

        return builder.create();

    }
}
