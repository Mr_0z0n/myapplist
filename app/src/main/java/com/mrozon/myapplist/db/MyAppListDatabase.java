package com.mrozon.myapplist.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.model.App;
import com.mrozon.myapplist.db.model.AppWithCategory;
import com.mrozon.myapplist.db.model.Category;
import com.mrozon.myapplist.db.model.CategoryWithCount;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import timber.log.Timber;

@Database(entities={App.class, Category.class}, views = {CategoryWithCount.class, AppWithCategory.class}, version=2)
public abstract class MyAppListDatabase extends RoomDatabase {

    public abstract MyAppListStore myAppListStore();
    public static final String DB_NAME="db_appslist";
    private static volatile MyAppListDatabase INSTANCE=null;

    public synchronized static MyAppListDatabase get(Context ctxt) {
        if (INSTANCE == null) {
            INSTANCE = create(ctxt, false);
        }
        return (INSTANCE);
    }

    public synchronized static MyAppListDatabase get() {
        if (INSTANCE == null) {
            INSTANCE = create(MyApplication.INSTANCE, false);
        }
        return (INSTANCE);
    }

    public static void reopen(){
        INSTANCE = create(MyApplication.INSTANCE, false);
    }

    private static MyAppListDatabase create(Context ctxt, boolean memoryOnly) {
        RoomDatabase.Builder<MyAppListDatabase> b;
        if (memoryOnly) {
            b = Room.inMemoryDatabaseBuilder(ctxt.getApplicationContext(),
                    MyAppListDatabase.class);
        }
        else {
            b = Room.databaseBuilder(ctxt.getApplicationContext(), MyAppListDatabase.class,
                    DB_NAME).addCallback(new Callback() {
                @Override
                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                    String sql="insert into mycategories (category_id, category_title) " +
                            "values (?,?)";
                    //add default category
                    db.execSQL(sql, new Object[]{1, MyApplication.INSTANCE.getString(R.string.no_category)});
                }
            });
        }
        b.addMigrations(Migrations.FROM_1_TO_2);
        return(b.build());
    }
}
