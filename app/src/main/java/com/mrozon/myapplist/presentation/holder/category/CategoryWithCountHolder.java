package com.mrozon.myapplist.presentation.holder.category;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mrozon.myapplist.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryWithCountHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.cat_title)
    public TextView cat_title;

    @BindView(R.id.cat_appscount)
    public TextView cat_appscount;

    @BindView(R.id.cat_image_delete)
    public ImageView cat_image_delete;

    @BindView(R.id.cat_image_rename)
    public ImageView cat_image_rename;

    public int category_id;

    public CategoryWithCountHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
