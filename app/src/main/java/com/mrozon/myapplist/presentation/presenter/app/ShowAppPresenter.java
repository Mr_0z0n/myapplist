package com.mrozon.myapplist.presentation.presenter.app;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.MyAppListDatabase;
import com.mrozon.myapplist.db.model.App;
import com.mrozon.myapplist.db.model.AppWithCategory;
import com.mrozon.myapplist.db.model.Category;
import com.mrozon.myapplist.presentation.view.app.ShowAppView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mrozon.myapplist.repositories.AppsRepository;
import com.mrozon.myapplist.utils.CommonUtils;
import com.snatik.storage.Storage;
import com.yandex.mobile.ads.AdRequest;
import com.yandex.mobile.ads.AdRequestError;
import com.yandex.mobile.ads.nativeads.NativeAdLoader;
import com.yandex.mobile.ads.nativeads.NativeAdLoaderConfiguration;
import com.yandex.mobile.ads.nativeads.NativeAppInstallAd;
import com.yandex.mobile.ads.nativeads.NativeContentAd;
import com.yandex.mobile.ads.nativeads.NativeImageAd;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.Subject;
import timber.log.Timber;

import static com.mrozon.myapplist.utils.AppUtils.openPlayStoreForApp;

@InjectViewState
public class ShowAppPresenter extends MvpPresenter<ShowAppView> {

    private List<Category> categories;
    private static CommonUtils.Variable<String> observableCategoryTitle;
    private static CommonUtils.Variable<Boolean> observableFavorite;
    private static CommonUtils.Variable<Boolean> observableInstallApp;

    private Flowable<String> observableFilterApps;
    private NativeAdLoader mNativeAdLoader;

    public ArrayList<String> getCategories() {
        ArrayList<String> categoriesTitle = new ArrayList<>();
        for (Category category: categories) {
            categoriesTitle.add(category.title);
        }
        return categoriesTitle;
    }

    public ShowAppPresenter() {
        observableCategoryTitle = new CommonUtils.Variable<>("");
        observableFavorite = new CommonUtils.Variable<>(false);
//        getViewState().changeFavoriteIcon(observableFavorite.getValue());
        observableInstallApp = new CommonUtils.Variable<>(true);
//        getViewState().changeInstallAppIcon(observableInstallApp.getValue());
    }

    private Flowable<List<App>> getApps(){
        AppsRepository appsRepository = AppsRepository.getInstance();
        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);

        return Flowable.combineLatest(db.myAppListStore().selectAppWithCategory(),appsRepository.getNonSystemApps2(),
                this::synchronizeApps)
                .observeOn(AndroidSchedulers.mainThread())
                .map(apps -> {
                    saveNewAppsInDb(apps);
                    return apps;
                });
    }

    private Completable insertApp(App app){
        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
        if(app.isNew)
            return Completable.fromAction(()->db.myAppListStore().insertApp(app));
        else
            return Completable.complete();
    }

    public void getItems() {

            getViewState().showProgress();

//        AppsRepository appsRepository = AppsRepository.getInstance();
//        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
//
//        Disposable disposable = Flowable.combineLatest(db.myAppListStore().selectAppWithCategory(),appsRepository.getNonSystemApps2(),
//                this::synchronizeApps)
//                .subscribeOn(Schedulers.computation())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(__ -> getViewState().showProgress())
//                .subscribe((apps) -> {
//                            getViewState().updateList(apps);
//                            saveNewAppsInDb(apps);
//                            getViewState().hideProgress();
////                            getViewState().showMessage(R.string.ok);
//                        },
//                        (throwable -> {
//                            getViewState().hideProgress();
//                            getViewState().showMessage(throwable.getMessage());
//                        }));

        Flowable<String> flowableCategory = observableCategoryTitle.getObservable().toFlowable(BackpressureStrategy.BUFFER);

        Flowable<Boolean> flowableFavorite = observableFavorite.getObservable().toFlowable(BackpressureStrategy.BUFFER);

        Flowable<Boolean> flowableInstalledApps = observableInstallApp.getObservable().toFlowable(BackpressureStrategy.BUFFER);


        //                    getViewState().updateList(apps);
        //                    getViewState().hideProgress();
        //                            getViewState().showMessage(R.string.ok);
//        Disposable subscribe1 = getApps().subscribe(this::saveNewAppsInDb,
//                (throwable -> {
////                    getViewState().hideProgress();
//                    getViewState().showMessage(throwable.getMessage());
//                }));

        Disposable subscribe = Flowable.combineLatest(getApps(), observableFilterApps, flowableCategory, flowableFavorite, flowableInstalledApps,
                this::filteredApps)
//                .flatMap(Flowable::fromIterable)
//                .filter(app -> app.title.toLowerCase().contains())
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe((apps) -> {
                            getViewState().updateList(apps);
                            getViewState().hideProgress();
//                            getViewState().showMessage(R.string.ok);
                        },
                        (throwable -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
    }

    private void saveNewAppsInDb(List<App> apps) {
        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
        Disposable subscribe = Flowable.fromIterable(apps)
                .subscribeOn(Schedulers.computation())

                .filter(app -> app.isNew)
                .toList()
                .flatMapCompletable((app) -> Completable.fromAction(() -> db.myAppListStore().insertApp(app.toArray(new App[0]))))
//        .blockingAwait()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe(() -> {
                            getViewState().hideProgress();
//                            getViewState().showMessage(R.string.ok);
                        },
                        (throwable -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
        ;


    }

    private List<App> synchronizeApps(List<AppWithCategory> from_db, List<App> from_device){
        Timber.d("%d %d",from_db.size(),from_device.size());
        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
        HashMap<String,App> apps1 = new HashMap<>();
        for (AppWithCategory appWithCategory:from_db) {
            appWithCategory.app.categoryTitle=appWithCategory.category.title;
            appWithCategory.app.isExisted = false;
            appWithCategory.app.isNew = false;
            apps1.put(appWithCategory.app.fullPackage,appWithCategory.app);
        }
        for (App app:from_device) {
            if(apps1.containsKey(app.fullPackage))
            {
                //app is install and exists in db
                App current = apps1.get(app.fullPackage);
                if(current.img==null || current.img!=app.img) {
                    App added = new App(current.id, current.title, current.fullPackage, app.img, current.favorite, current.comment, current.fk_category);
                    added.categoryTitle = current.categoryTitle;
                    added.isExisted = true;
                    apps1.remove(current);
                    apps1.put(added.fullPackage,added);
                }
            }
            else
            {
                //app is install and NOT exists in db
                app.categoryTitle=MyApplication.INSTANCE.getString(R.string.no_category);
                app.isExisted = true;
                apps1.put(app.fullPackage,app);
//                db.myAppListStore().insertApp(app);
//                Completable.fromAction(()->db.myAppListStore().insertApp(app)).blockingAwait();
                app.isNew = true;
            }
        }
//        SortedSet<String> keys = new TreeSet<>(apps1.keySet());

        List<App> result = new ArrayList<App>(apps1.size());
        for (String key:apps1.keySet() ) {
            result.add(apps1.get(key));
        }
        Collections.sort(result, (o1, o2) -> o1.title.compareToIgnoreCase(o2.title));
        return result;
    }

    private void changeApp(App app, int res_message){
        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
        Disposable disposable = Completable.fromAction(() -> db.myAppListStore().updateApp(app))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe(() -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(res_message);
                        },
                        (throwable -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
    }

    public void changeFavorite(App app) {
        app.favorite = !app.favorite;
        changeApp(app,app.favorite?R.string.app_add_favotite:R.string.app_remove_favotite);
    }

    public void deleteComment(App app) {
        app.comment="";
        changeApp(app,R.string.app_delete_comments);
    }

    public void changeComment(App app, String comment) {
        app.comment=comment;
        changeApp(app, R.string.comment_changed);
    }

    public void selectAllCategories() {
        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
        final Disposable items = db.myAppListStore().selectCategories()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__-> getViewState().showProgress())
//                .doOnTerminate(()->getViewState().hideProgress())
//                .doFinally(()->getViewState().hideProgress())
                .subscribe((List<Category> categories) -> {
                    this.categories = categories;
                    getViewState().hideProgress();
                }, throwable -> {
                    getViewState().hideProgress();
                    getViewState().showMessage(throwable.getMessage());
                });
    }

    public void changeCategory(App app, String categoryTitle) {
        if(categories!=null)
        {
            for (Category category: categories) {
                if(category.title.equals(categoryTitle))
                {
                    app.fk_category = category.id;
                    app.categoryTitle = category.title;
                    changeApp(app, R.string.app_category_changed);
                    break;
                }
            }
        }
    }

    public void filterApps(EditText etSearchApp) {
         observableFilterApps = RxTextView.textChanges(etSearchApp)
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(Schedulers.computation())
                .map(CharSequence::toString)
                .map(String::toLowerCase)
//                 .filter(s->!s.isEmpty())
                 .toFlowable(BackpressureStrategy.BUFFER);

//        Flowable<String> flowableCategory = observableCategoryTitle.getObservable().toFlowable(BackpressureStrategy.BUFFER);
//
//        Flowable<Boolean> flowableFavorite = observableFavorite.getObservable().toFlowable(BackpressureStrategy.BUFFER);
//
//        Flowable<Boolean> flowableInstalledApps = observableInstallApp.getObservable().toFlowable(BackpressureStrategy.BUFFER);

//        Disposable subscribe = Flowable.combineLatest(getApps(), observableFilterApps, flowableCategory, flowableFavorite, flowableInstalledApps,
//                this::filteredApps)
////                .flatMap(Flowable::fromIterable)
////                .filter(app -> app.title.toLowerCase().contains())
//                .subscribeOn(Schedulers.computation())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(__ -> getViewState().showProgress())
//                .subscribe((apps) -> {
//                            getViewState().updateList(apps);
//                            getViewState().hideProgress();
////                            getViewState().showMessage(R.string.ok);
//                        },
//                        (throwable -> {
//                            getViewState().hideProgress();
//                            getViewState().showMessage(throwable.getMessage());
//                        }));

    }

    private List<App> filteredApps(List<App> apps, String filter, String category, Boolean favorite, Boolean installedApps){
//        if(filter.isEmpty())
//            return apps;
//        saveNewAppsInDb(apps);
//        MyAppListDatabase db = MyAppListDatabase.get(MyApplication.INSTANCE);
        List<App> filtered = new ArrayList<>();
        for (App app: apps) {
//            if(app.isNew)
//                db.myAppListStore().insertApp(app);
//            if(app.title.toLowerCase().contains(filter) && app.categoryTitle.equals(category))
            if(!filter.isEmpty() && !app.title.toLowerCase().contains(filter))
                continue;
            if(!category.isEmpty() && !app.categoryTitle.contains(category))
                continue;
            if(favorite)
                if(app.favorite.equals(false))
                    continue;
            if(app.isExisted!=installedApps)
                continue;

            filtered.add(app);
        }
        return filtered;
    }

    public void setObservableCategoryTitle(String categoryTitle) {
        observableCategoryTitle.setValue(categoryTitle);
    }

    public void setObservableFavoriteMenu() {
        observableFavorite.setValue(!observableFavorite.getValue());
//        Timber.d(observableFavorite.getValue().toString());
        getViewState().changeFavoriteIcon(observableFavorite.getValue());
    }

    public void setObservableInstallAppMenu() {
        observableInstallApp.setValue(!observableInstallApp.getValue());
        getViewState().changeInstallAppIcon(observableInstallApp.getValue());
    }

    public void menuReady() {
        getViewState().changeFavoriteIcon(observableFavorite.getValue());
        getViewState().changeInstallAppIcon(observableInstallApp.getValue());
    }

    public void downloadApk(App app) {
        Context context = MyApplication.INSTANCE;
        PackageManager pm = context.getPackageManager();
        try {
            ApplicationInfo ai = pm.getApplicationInfo( app.fullPackage, 0);
            String original = ai.sourceDir;
            PackageInfo pi = pm.getPackageInfo(app.fullPackage, 0);
            String copy = app.title.replace(' ','_')+"_"+pi.versionName+ ".apk";

            Storage storage = new Storage(MyApplication.INSTANCE);
            String newDir = storage.getExternalStorageDirectory() + File.separator + context.getString(R.string.dir_app_name);
            if(!storage.isDirectoryExists(newDir))
                storage.createDirectory(newDir);

            boolean result = storage.copy(original,newDir + File.separator + copy);
//            Timber.d("result copy %s",result);
            if(result)
                getViewState().showMessageWithOpenAction(R.string.apk_downloaded_success,newDir /*+ File.separator + copy*/);
            else
                getViewState().showMessage(R.string.apk_downloaded_failed);

        } catch (PackageManager.NameNotFoundException e) {
            Timber.e(e);
            getViewState().showMessage(e.getMessage());
        }





    }

    public void createNativeAdLoader() {
        //R-M-266381-1
        final NativeAdLoaderConfiguration adLoaderConfiguration =
                new NativeAdLoaderConfiguration.Builder("R-M-266381-1", false)
                        .setImageSizes(NativeAdLoaderConfiguration.NATIVE_IMAGE_SIZE_SMALL).build();
        mNativeAdLoader = new NativeAdLoader(MyApplication.INSTANCE, adLoaderConfiguration);
        mNativeAdLoader.setNativeAdLoadListener(new NativeAdLoader.OnImageAdLoadListener() {
            @Override
            public void onImageAdLoaded(@NonNull NativeImageAd nativeImageAd) {

            }

            @Override
            public void onAdFailedToLoad(@NonNull AdRequestError adRequestError) {
                Timber.e(adRequestError.getDescription());
            }

            @Override
            public void onAppInstallAdLoaded(@NonNull NativeAppInstallAd nativeAppInstallAd) {

            }

            @Override
            public void onContentAdLoaded(@NonNull NativeContentAd nativeContentAd) {
                getViewState().loadAd(nativeContentAd);
            }
        });
    }

    public void loadAds(int ads_capacity) {
        if(ads_capacity<1)
            return;
        for (int i=0;i<ads_capacity;i++)
            mNativeAdLoader.loadAd(AdRequest.builder().build());
    }
}
