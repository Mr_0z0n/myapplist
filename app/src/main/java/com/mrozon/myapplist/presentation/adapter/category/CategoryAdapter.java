package com.mrozon.myapplist.presentation.adapter.category;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.model.Category;
import com.mrozon.myapplist.db.model.CategoryWithCount;
import com.mrozon.myapplist.presentation.holder.category.CategoryHolder;
import com.mrozon.myapplist.presentation.holder.category.CategoryWithCountHolder;
import com.mrozon.myapplist.utils.rvpattern.RVAdapter;

public class CategoryAdapter extends RVAdapter {

    private ClickListener onDeleteClickListener;
    private ClickListener onEditClickListener;

    public interface ClickListener {
        void onClick(int category_id, String text);
    }

    public CategoryAdapter(ClickListener onDeleteClickListener, ClickListener onEditClickListener) {
        this.onDeleteClickListener = onDeleteClickListener;
        this.onEditClickListener = onEditClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryWithCountHolder(inflateByViewType(MyApplication.INSTANCE,viewType,parent));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CategoryWithCountHolder){
            CategoryWithCount categoryWithCount = (CategoryWithCount)items.get(position);
            CategoryWithCountHolder categoryWithCountHolder = (CategoryWithCountHolder)holder;
            categoryWithCountHolder.category_id = categoryWithCount.category_id;
            boolean isSystemCategory = categoryWithCount.category_id==1;
            categoryWithCountHolder.cat_image_delete.setVisibility(isSystemCategory? View.INVISIBLE:View.VISIBLE);
            categoryWithCountHolder.cat_image_delete.setEnabled(!isSystemCategory);
            categoryWithCountHolder.cat_image_rename.setVisibility(isSystemCategory? View.INVISIBLE:View.VISIBLE);
            categoryWithCountHolder.cat_image_rename.setEnabled(!isSystemCategory);
            categoryWithCountHolder.cat_title.setText(categoryWithCount.category_title);
            categoryWithCountHolder.cat_appscount.setText(MyApplication.INSTANCE.getString(R.string.cat_title_counts_app,String.valueOf(categoryWithCount.count)));
            categoryWithCountHolder.cat_image_delete.setOnClickListener(l->{
                if(onDeleteClickListener!=null)
                    onDeleteClickListener.onClick(categoryWithCount.category_id, categoryWithCount.category_title);
            });
            categoryWithCountHolder.cat_image_rename.setOnClickListener(l->{
                if(onEditClickListener!=null)
                    onEditClickListener.onClick(categoryWithCount.category_id, categoryWithCount.category_title);
            });
        }
    }
}
