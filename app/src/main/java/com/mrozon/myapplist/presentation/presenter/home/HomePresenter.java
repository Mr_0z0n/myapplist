package com.mrozon.myapplist.presentation.presenter.home;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.Screens;
import com.mrozon.myapplist.presentation.view.home.HomeView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mrozon.myapplist.ui.activity.home.HomeActivity;
import com.mrozon.myapplist.ui.fragment.rate.RateDialogFragment;

import java.util.Objects;

import ru.terrakok.cicerone.Router;
import timber.log.Timber;

import static com.mrozon.myapplist.ui.activity.home.HomeActivity.RC_SIGN_IN;

@InjectViewState
public class HomePresenter extends MvpPresenter<HomeView> {

    private Router router;
    private Activity activity;

    public HomePresenter() {
        router = MyApplication.INSTANCE.getRouter();
    }

    public void sign(HomeActivity activity, GoogleSignInClient mGoogleSignInClient, FirebaseAuth mAuth) {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser==null){
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            this.activity = activity;
            activity.startActivityForResult(signInIntent, RC_SIGN_IN);
            getViewState().closeDrawer();
        }
        else
        {
            mGoogleSignInClient.signOut();
            mAuth.signOut();
            getViewState().updateUserInfo(null);
            getViewState().closeDrawer();
            getViewState().showError(R.string.log_out);
        }
    }

    public void onFirstShowFragment() {
        router.newRootChain(new Screens.ShowApps());
    }

    public void getResultGoogleSignIn(Task<GoogleSignInAccount> task, FirebaseAuth mAuth) {
        try {
            // Google Sign In was successful, authenticate with Firebase
            GoogleSignInAccount account = task.getResult(ApiException.class);
            if(account==null)
            {
                Timber.e("Google account is null");
                getViewState().showError("Google account is null");
                return;
            }
            firebaseAuthWithGoogle(account, mAuth);
        } catch (ApiException e) {
            Timber.e(e,"Google sign in failed");
            getViewState().showError(e.getMessage());
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct, final FirebaseAuth mAuth) {

        Timber.d("firebaseAuthWithGoogle: %s", acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(activity, task -> {
            if (task.isSuccessful()) {
                // Sign in success, updateIndicatorType UI with the signed-in user's information
                Timber.d("signInWithCredential:success");
                FirebaseUser user = mAuth.getCurrentUser();
                if (user != null) {
                    //getViewState().showError(user.getDisplayName());
                    getViewState().showGreating(activity.getString(R.string.greating,user.getDisplayName()));
                }
                getViewState().updateUserInfo(user);
            } else {
                // If sign in fails, display a message to the user.
                Timber.e(task.getException(),"signInWithCredential:failure");
                getViewState().showError("Authentication Failed: "+ Objects.requireNonNull(task.getException()).getMessage());
            }
        });

    }


    public void onShowCategories() {
        router.navigateTo(new Screens.ShowCategories());
    }

    public void onShowApps() {
        router.newRootScreen(new Screens.ShowApps());
    }

    public void onShowSaveBackup() {
        router.navigateTo(new Screens.ShowSaveBackup());
    }

    public void onShowRestoreBackup() {
        router.navigateTo(new Screens.ShowRestoreBackup());
    }

}
