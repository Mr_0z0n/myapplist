package com.mrozon.myapplist.repositories;

import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.db.MyAppListDatabase;
import com.snatik.storage.Storage;

import static com.mrozon.myapplist.db.MyAppListDatabase.DB_NAME;

public class DbBackupRestore implements IDbBackupRestore {

    private static DbBackupRestore INSTANCE;

    public static DbBackupRestore getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DbBackupRestore();
        }
        return INSTANCE;
    }

    @Override
    public void backup(String file) {
        MyAppListDatabase.get().close();
        Storage storage = new Storage(MyApplication.INSTANCE);
        String from = MyApplication.INSTANCE.getDatabasePath(DB_NAME).getAbsolutePath();
        storage.copy(from,file);
        MyAppListDatabase.reopen();
    }

    @Override
    public void restore(String backup_file) {
        MyAppListDatabase.get().close();
        Storage storage = new Storage(MyApplication.INSTANCE);
        String to = MyApplication.INSTANCE.getDatabasePath(DB_NAME).getAbsolutePath();
        storage.copy(backup_file,to);
        MyAppListDatabase.reopen();
    }
}
