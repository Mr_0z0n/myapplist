package com.mrozon.myapplist.db.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.mrozon.myapplist.R;
import com.mrozon.myapplist.utils.rvpattern.IBaseListItem;

import java.io.Serializable;

@Entity(tableName = "mycategories")
public class Category implements IBaseListItem {

    @PrimaryKey(autoGenerate=true)
    @ColumnInfo(name="category_id")
    public final int id;
    @ColumnInfo(name="category_title")
    public final String title;

    public Category(int id, String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_category;
    }
}
