package com.mrozon.myapplist.db.model;

import android.graphics.drawable.Drawable;

import androidx.room.ColumnInfo;
import androidx.room.DatabaseView;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.BlobTransmogrifier;
import com.mrozon.myapplist.utils.rvpattern.IBaseListItem;

import static androidx.room.ForeignKey.SET_DEFAULT;

@DatabaseView("select myapps.*, mycategories.* " +
        "from myapps, mycategories where app_category=category_id")
public class AppWithCategory implements IBaseListItem {

    @Embedded
    public App app;
    @Embedded
    public Category category;

    @Override
    public int getLayoutId() {
        return R.layout.item_app;
    }
}