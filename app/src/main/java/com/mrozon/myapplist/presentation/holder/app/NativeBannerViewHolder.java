package com.mrozon.myapplist.presentation.holder.app;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mrozon.myapplist.R;
import com.yandex.mobile.ads.nativeads.template.NativeBannerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NativeBannerViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.native_template)
    public NativeBannerView nativeBannerView;

    public NativeBannerViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
