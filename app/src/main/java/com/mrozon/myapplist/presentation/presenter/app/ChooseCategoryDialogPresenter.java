package com.mrozon.myapplist.presentation.presenter.app;

import com.mrozon.myapplist.presentation.view.app.ChooseCategoryDialogView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

@InjectViewState
public class ChooseCategoryDialogPresenter extends MvpPresenter<ChooseCategoryDialogView> {

}
