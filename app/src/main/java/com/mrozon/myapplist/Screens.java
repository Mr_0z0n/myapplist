package com.mrozon.myapplist;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.mrozon.myapplist.ui.fragment.app.ShowAppFragment;
import com.mrozon.myapplist.ui.fragment.backup.RestoreBackupFragment;
import com.mrozon.myapplist.ui.fragment.backup.SaveBackupFragment;
import com.mrozon.myapplist.ui.fragment.category.ShowCategoryFragment;

import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class Screens {
//    public static final class CommentsList extends SupportAppScreen {
//
//        private String liveChatId;
//        private String interval;
//        private Boolean auto;
//
//        public CommentsList(String liveChatId, String interval, Boolean auto) {
//            this.liveChatId = liveChatId;
//            this.interval = interval;
//            this.auto = auto;
//        }
//
//        public CommentsList(FragmentActivity activity) {
//            SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
//            this.liveChatId = sharedPref.getString("liveChatId","");
//            this.interval = String.valueOf(sharedPref.getInt("interval",1));
//            this.auto = sharedPref.getBoolean("autoDetectBugs",true);
//        }
//
//        @Override
//        public Fragment getFragment() {
//            return  CommentsListFragment.newInstance(liveChatId, interval, auto);
//        }
//    }

    public static final class ShowCategories extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return  ShowCategoryFragment.newInstance();
        }
    }

    public static final class ShowApps extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return  ShowAppFragment.newInstance();
        }
    }

    public static final class ShowSaveBackup extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return  SaveBackupFragment.newInstance();
        }
    }

    public static final class ShowRestoreBackup extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return  RestoreBackupFragment.newInstance();
        }
    }

}
