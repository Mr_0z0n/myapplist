package com.mrozon.myapplist.presentation.view.backup;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface SaveBackupView extends MvpView {

    @StateStrategyType(SkipStrategy.class)
    void showMessage(int res_string);

    @StateStrategyType(SkipStrategy.class)
    void showMessage(String message);

    void showProgress();

    void hideProgress();
}
