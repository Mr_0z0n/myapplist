package com.mrozon.myapplist.presentation.presenter.rate;

import com.mrozon.myapplist.presentation.view.rate.RateDialogView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

@InjectViewState
public class RateDialogPresenter extends MvpPresenter<RateDialogView> {

}
