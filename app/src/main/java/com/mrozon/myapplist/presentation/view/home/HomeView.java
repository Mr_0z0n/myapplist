package com.mrozon.myapplist.presentation.view.home;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.google.firebase.auth.FirebaseUser;

public interface HomeView extends MvpView {

    void closeDrawer();

    void updateUserInfo(FirebaseUser currentUser);

    @StateStrategyType(SkipStrategy.class)
    void showError(int mes_id);

    @StateStrategyType(SkipStrategy.class)
    void showError(String message);

    @StateStrategyType(SkipStrategy.class)
    void showGreating(String message);
}
