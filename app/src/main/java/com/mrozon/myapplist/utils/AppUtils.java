package com.mrozon.myapplist.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.db.model.App;

import static com.mrozon.myapplist.utils.AppConstants.APP_GOOGLE_PLAY_STORE_LINK;
import static com.mrozon.myapplist.utils.AppConstants.APP_MARKET_LINK;

public final class AppUtils {

    private AppUtils() {
        // This class is not publicly instantiable
    }

    public static void openPlayStoreForApp(String appPackageName) {
        Context context = MyApplication.INSTANCE;
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(APP_MARKET_LINK + appPackageName))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (android.content.ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(APP_GOOGLE_PLAY_STORE_LINK + appPackageName))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    public static void openPlayStoreForApp() {
        Context context = MyApplication.INSTANCE;
        String appPackageName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(APP_MARKET_LINK + appPackageName))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (android.content.ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(APP_GOOGLE_PLAY_STORE_LINK + appPackageName))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

//    public static void openPrivacyPolicyForApp(Context context) {
//        Intent i = new Intent(Intent.ACTION_VIEW);
//        i.setData(Uri.parse(context.getString(R.string.privacy_policy)));
//        context.startActivity(i);
//    }

    public static void shareApp(App app){
        Context context = MyApplication.INSTANCE;
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, app.title);
        String sAux = "\n"+context.getResources().getString(R.string.share_app_prompt)+"\n\n";
        sAux = sAux + "https://play.google.com/store/apps/details?id="+app.fullPackage+" \n\n";
        i.putExtra(Intent.EXTRA_TEXT, sAux);
        context.startActivity(i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public static void openPrivacyPolicyForApp(Context context) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(context.getString(R.string.privacy_policy)));
        context.startActivity(i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

}
