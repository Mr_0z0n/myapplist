package com.mrozon.myapplist.presentation.presenter.backup;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mrozon.myapplist.BuildConfig;
import com.mrozon.myapplist.MyApplication;
import com.mrozon.myapplist.R;
import com.mrozon.myapplist.presentation.view.backup.SaveBackupView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mrozon.myapplist.repositories.DbBackupRestore;
import com.snatik.storage.Storage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import durdinapps.rxfirebase2.RxFirebaseStorage;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.mrozon.myapplist.db.MyAppListDatabase.DB_NAME;
import static com.mrozon.myapplist.utils.AppConstants.TIMESTAMP_FILE_FORMAT;

@InjectViewState
public class SaveBackupPresenter extends MvpPresenter<SaveBackupView> {

    public void saveLocalBackup(Context context) {
//        Context context = MyApplication.INSTANCE;
        DialogProperties properties = new DialogProperties();
        properties.selection_type = DialogConfigs.DIR_SELECT;
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;
        FilePickerDialog dialog = new FilePickerDialog(context, properties);
        dialog.setTitle(context.getString(R.string.select_dir_backup));
        dialog.setPositiveBtnName(context.getString(R.string.ok));
        dialog.setNegativeBtnName(context.getString(R.string.cancel));
        dialog.setDialogSelectionListener(files -> {
            SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_FILE_FORMAT);
            if(files.length<1)
            {
                getViewState().showMessage(R.string.file_not_selected);
                return;
            }
            String backup_file = String.format("%s/%s_%s.backup", files[0], DB_NAME, sdf.format(new Date()));
            Disposable subscribe = Completable.fromAction(() -> DbBackupRestore.getInstance().backup(backup_file))
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(__ -> getViewState().showProgress())
                    .subscribe(() -> {
                                getViewState().hideProgress();
                                getViewState().showMessage(R.string.backup_completed);
                            },
                            (throwable -> {
                                getViewState().hideProgress();
                                getViewState().showMessage(throwable.getMessage());
                            }));
        });
        dialog.show();
    }

    public void shareLocalBackup() {
        Storage storage = new Storage(MyApplication.INSTANCE);
        Context context = MyApplication.INSTANCE;
        String dir_app_name = MyApplication.INSTANCE.getString(R.string.dir_app_name);
        if(!storage.isDirectoryExists(Environment.getExternalStoragePublicDirectory(dir_app_name).getAbsolutePath())){
            storage.createDirectory(Environment.getExternalStoragePublicDirectory(dir_app_name).getAbsolutePath());
        }
        String dirPath = Environment.getExternalStoragePublicDirectory(dir_app_name).getAbsolutePath()+ "/Share";
        if(!storage.isDirectoryExists(dirPath)){
            storage.createDirectory(dirPath);
        }
        String backup_file = String.format("%s/%s.backup", dirPath, DB_NAME);
        if(storage.isFileExist(backup_file))
            storage.deleteFile(backup_file);
        Disposable subscribe = Completable.fromAction(() -> DbBackupRestore.getInstance().backup(backup_file))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe(() -> {

                            Uri uri;
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                                uri = Uri.fromFile(new File(backup_file));
                            } else {
                                uri = FileProvider.getUriForFile(context,
                                        BuildConfig.APPLICATION_ID + ".provider",
                                        new File(backup_file));
                            }

                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");

                            intent.putExtra(Intent.EXTRA_SUBJECT, "");
                            intent.putExtra(Intent.EXTRA_TEXT, "");
                            intent.putExtra(Intent.EXTRA_STREAM, uri);

                            getViewState().hideProgress();
                            try {
                                context.startActivity(Intent.createChooser(intent, context.getString(R.string.share_backup))
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            } catch (ActivityNotFoundException ex) {
                                ex.printStackTrace();
                            }
                        },
                        (throwable -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
    }

    public void saveCloudBackup() {
        if(FirebaseAuth.getInstance().getUid()==null)
        {
            getViewState().showMessage(R.string.firebase_auth_null);
            return;
        }
        Storage storage = new Storage(MyApplication.INSTANCE);
        String dir_app_name = MyApplication.INSTANCE.getString(R.string.dir_app_name);
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        StorageReference riversRef = storageReference.child(String.format("user/%s/backup/%s.backup", FirebaseAuth.getInstance().getUid(), DB_NAME));

        if(!storage.isDirectoryExists(Environment.getExternalStoragePublicDirectory(dir_app_name).getAbsolutePath())){
            storage.createDirectory(Environment.getExternalStoragePublicDirectory(dir_app_name).getAbsolutePath());
        }
        String dirPath = Environment.getExternalStoragePublicDirectory(dir_app_name).getAbsolutePath()+ "/Share";
        if(!storage.isDirectoryExists(dirPath)){
            storage.createDirectory(dirPath);
        }
        String backup_file = String.format("%s/%s.backup", dirPath, DB_NAME);
        if(storage.isFileExist(backup_file))
            storage.deleteFile(backup_file);

        Disposable subscribe = Single.fromCallable(() -> {
            DbBackupRestore.getInstance().backup(backup_file);
            return new Object();
        }).subscribeOn(Schedulers.computation())
                .flatMap((Function<Object, SingleSource<UploadTask.TaskSnapshot>>) o -> {
                    File file = new File(backup_file);
                    Uri uri = Uri.fromFile(file);
                    return RxFirebaseStorage.putFile(riversRef, uri);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe(snapshot -> {
                    getViewState().hideProgress();
                    getViewState().showMessage(R.string.backup_upload);
                    Timber.d("saveCloudBackup: transferred " + snapshot.getBytesTransferred() + " bytes");
                }, throwable -> {
                    getViewState().hideProgress();
                    Timber.d("saveCloudBackup: %s", throwable.toString());
                    getViewState().showMessage(throwable.toString());
                });
    }
}
